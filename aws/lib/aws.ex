defmodule Aws do
  defdelegate list_container(container), to: Aws.Blob
  defdelegate put_blob(container, file, blob_name), to: Aws.Blob
  defdelegate sign_url(http_method, url, region, service), to: Aws.Sign
end
