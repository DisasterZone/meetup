export default {
    oidc: {
        authority: 'https://capgroup-dev.oktapreview.com',
        clientId: '0oab06trttuR7LU1d0h7',
        issuer: 'https://capgroup-dev.oktapreview.com',
        redirectUri: 'http://localhost:4200/auth',
        responseType: 'id_token',
        scope: 'openid profile',
        filterProtocolClaims: true,
        loadUserInfo: true
    },
    resourceServer: {
        messagesUrl: 'http://localhost:8000/api/messages',
    },
};