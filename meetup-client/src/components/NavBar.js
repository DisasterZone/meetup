import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import { withAuth } from '@okta/okta-react';
import { checkAuthentication } from './helpers';

export default withAuth(class NavBar extends Component {
    constructor(props) {
        super(props);
        this.state = { authenticated: null };
        this.checkAuthentication = checkAuthentication.bind(this);
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
    }

    async componentDidMount() {
        this.checkAuthentication();
    }
    
    async componentDidUpdate() {
        this.checkAuthentication();
    }
    
    async login() {
        this.props.auth.login('/');
    }
    
    async logout() {
        this.props.auth.logout('/');
    }

    render() {
        return(
            <div>
                <AppBar position="static">
                    <Toolbar>
                        <Typography variant="title" color="inherit">
                            CG Meetup
                        </Typography>
                        {this.state.authenticated === true && 'Messages'}
                        {this.state.authenticated === false && 'Out'}
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
});