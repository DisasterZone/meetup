import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Event from '../components/Event';

import { withAuth } from '@okta/okta-react';
import { checkAuthentication } from './helpers';

export default withAuth(class EventList extends Component {
    state = {
        events: [],
        searchString: ''
    }
    
    constructor(props) {
        super(props);
        this.state = {
            authenticated: null,
            events: [],
            searchString: '' };
        this.checkAuthentication = checkAuthentication.bind(this);
        this.getEvents(this.props);
    }

    getEvents = (props) => {
        console.log(props);
        console.log(props.auth.getAccessToken());
        console.log(props.auth.getIdToken());
        console.log(props.auth.getUser());

        /*, {
            headers: {
                Authorization: 'Bearer ' + this.props.auth.getAccessToken()
            },
        }*/

        fetch('http://localhost:4000/api/events')
        .then((response) => {
            return response.json();
        })
        .then((eventJson) => {
            console.log(JSON.stringify(eventJson));
            this.setState({events: eventJson});
        })
        .catch((error) => {
            console.log("Error occurred while fetching Events");
            console.error(error);
        });
    }

    onSearchInputChange = (event) => {
        console.log("Search changed ..." + event.target.value);
        if (event.target.value) {
            this.setState({searchString: event.target.value});
        } else {
            this.setState({searchString: ''});
        }
        this.getEvents();
    }

    render() {
        return (
            <div>
                { this.state.events ? (
                    <div>
                        <TextField style={{padding: 24}}
                            id="searchInput"
                            placeholder="Search for Events"   
                            margin="normal"
                            onChange={this.onSearchInputChange}
                        />
                        <Grid container spacing={24} style={{padding: 24}}>
                            { this.state.events.map(currentEvent => (
                                <Grid key={currentEvent.id} item xs={12} sm={6} lg={4} xl={3}>
                                    <Event event={currentEvent} />
                                </Grid>
                            ))}
                        </Grid>
                    </div>
                ) : "No events found" }
            </div>
        )
    }
});
