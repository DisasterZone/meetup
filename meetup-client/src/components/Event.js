import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Moment from 'react-moment';

const Event = (props) => {
    console.log("events");
    console.log(props);
    return (
        <div>
            { props.event ? (
                <Card >
                    <Typography variant="headline" component="h3"
                        style={{paddingLeft: 20, paddingTop: 20, paddingBottom: 10}}>
                        {props.event.title}
                    </Typography>
                    <CardMedia style={{height: 0, paddingTop: '56.25%'}}
                        image={props.event.imageUrl}
                        title={props.event.title}
                    />
                    <CardContent style={{display: 'flex', flexWrap: 'wrap'}}>
                        <Typography component="p">
                            <strong>Event Date:</strong>
                            <Moment format="MM/DD/YYYY">
                                {props.event.startTime}
                            </Moment>
                        </Typography>
                        <Typography align='right'>
                            <strong>Organizer:</strong> {props.event.organizer}
                        </Typography>
                        <Typography component="p">
                            <strong>Start Time:</strong>
                            <Moment format="HH:mm">
                                {props.event.startTime}
                            </Moment>
                        </Typography>
                        <Typography component="p">
                            {props.event.description}
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button size="small" color="primary" href='' target="_blank">
                            Go To Event
                        </Button>
                    </CardActions>
                </Card>
            ) : null}
        </div>
    );
}

export default Event