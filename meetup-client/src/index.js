import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Security } from '@okta/okta-react';

import './index.css';
import config from './.meetup.config';

import App from './App';
import registerServiceWorker from './registerServiceWorker';

function onAuthRequired( { history }) {
    history.push('/login');
}

ReactDOM.render(
    <Router>
        <Security
            authority={ config.oidc.authority }
            issuer={ config.oidc.issuer }
            client_id={ config.oidc.clientId }
            redirect_uri={ config.oidc.redirectUri }
            response_type={ config.oidc.responseType }
            scope={ config.oidc.scope }
            onAuthRequired={onAuthRequired}>
            <App />
        </Security>
    </Router>,
    document.getElementById('root')
);
registerServiceWorker();
