import React, { Component } from 'react';
import NavBar from './components/NavBar';
import EventList from './components/EventList';

import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Security, SecureRoute, ImplicitCallback } from '@okta/okta-react';
import config from './.meetup.config';

class App extends Component {

    render() {
        return (
            <Router>
                <Security
                    authority={ config.oidc.authority }
                    issuer={ config.oidc.issuer }
                    client_id={ config.oidc.clientId }
                    redirect_uri={ config.oidc.redirectUri }
                    response_type={ config.oidc.responseType }
                >
                    <NavBar />
                    <EventList />
                    <SecureRoute path="/events" component={EventList} />
                </Security>
            </Router>
        );
    }
};

export default App;