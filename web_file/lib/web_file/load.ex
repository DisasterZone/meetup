defmodule WebFile.Load do
  def load_file(path, name) do
    File.read("#{path}/#{name}")
  end
end
