defmodule WebFile.Azure do
  def put_file(container, name, file_path) do
    case ExAzure.request(:put_block_blob, [container, name, file_path]) do
      {:ok, {:ok, :created}} -> build_url(container, name)
      _ -> {:error, "Error uploading file to Azure."}
    end
  end

  def delete_file(container, name) do
    ExAzure.request(:delete_blob, [container, name])
  end

  defp build_url(container, name) do
    "https://capmeetup.blob.core.windows.net/#{container}/#{name}"
  end
end
