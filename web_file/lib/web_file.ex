defmodule WebFile do
  alias WebFile.Azure

  defdelegate put_file(container, name, file_path), to: Azure
  defdelegate delete_file(container, name), to: Azure
end
