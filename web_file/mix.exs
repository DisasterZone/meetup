defmodule WebFile.Mixfile do
  use Mix.Project

  def project do
    [
      app: :web_file,
      version: "0.1.0",
      elixir: "~> 1.5",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :ex_azure]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:erlazure, github: "dkataskin/erlazure"},
      {:ex_azure, "~> 0.1.1"}
    ]
  end
end
