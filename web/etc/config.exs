use Mix.Config

config :web, WebWeb.Endpoint,
  cache_static_manifest: "priv/static/cache_manifest.json",
  server: true,
  root: ".",
  version: Application.spec(:phoenix_distillery, :vsn)

config :db, ecto_repos: [Db.Repo]
  config :db, Db.Repo,
    adapter: Ecto.Adapters.Postgres,

config :ex_aws,
  access_key_id:     ${"CG_MEETUP_AWS_ACCESS_KEY_ID"},
  secret_access_key: ${"CG_MEETUP_AWS_SECRET_ACCESS_KEY"},
  s3: [
    scheme: "https://",
    host: "meetup-profiles.s3.amazonaws.com",
    region: "us-west-1"
  ]
