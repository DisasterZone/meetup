# Web

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`
  * Mix.Tasks.Phx.Routes.run ''
  * source .env; elixir --detached -S mix phx.server
  * sudo netstat -plnt
  * 
  * ssh -i "meetup.pem" ubuntu@ec2-54-202-15-110.us-west-2.compute.amazonaws.com
  * ssh -i "meetup-cg.pem" ubuntu@ec2-13-57-39-14.us-west-1.compute.amazonaws.com

  *  1  wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb
  *  2  sudo dpkg -i erlang-solutions_1.0_all.deb
  *  3  sudo apt-get update
  *  4  sudo apt-get install esl-erlang
  *  5  sudo apt-get install elixir
  *  6  mix local.hex
  *  7  mix archive.install https://github.com/phoenixframework/archives/raw/master/1.4-dev/phx_new.ez

  *  0  bitbucket clone - git clone https://DisasterZone@bitbucket.org/DisasterZone/meetup.git
  *  1  mix deps.get
  *  2  cd assets; npm i; cd ..
  *  3  cd assets; node node_modules/webpack/bin/webpack.js --mode development; cd ..
  *  4  mix phx.digest
  *  5  MIX_ENV=prod mix compile
  *  6  # MIX_ENV=prod mix phx.server
  *  7  MIX_ENV=prod mix release --env=prod

  *  8  . ./.env; _build/prod/rel/web/bin/web console
  * . ./.env; iex -S mix phx.server
  * . ./.env _build/dev/rel/web/bin/web start
  * . ./.env _build/dev/rel/web/bin/web stop

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Read Configuration

  * Application.get_env(:db, Db.Repo)
  * Application.get_env(:web, WebWeb.Endpoint)[:http]
  * ExAws.Config.new(:s3)

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix

## Release Command

  * > CG_MEETUP_PORT=4200 CG_MEETUP_HOST=localhost _build/prod/rel/web/bin/web foreground
  * > source .env _build/prod/rel/web/bin/web foreground

To start the release you have built, you can use one of the following tasks:

    # start a shell, like 'iex -S mix'
    > _build/prod/rel/web/bin/web console

    # start in the foreground, like 'mix run --no-halt'
    > _build/prod/rel/web/bin/web foreground

    # start in the background, must be stopped with the 'stop' command
    > _build/prod/rel/web/bin/web start

If you started a release elsewhere, and wish to connect to it:

    # connects a local shell to the running node
    > _build/prod/rel/web/bin/web remote_console

    # connects directly to the running node's console
    > _build/prod/rel/web/bin/web attach

For a complete listing of commands and their use:

    > _build/prod/rel/web/bin/web help