use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :web, WebWeb.Endpoint,
  http: [:inet6, port: System.get_env("CG_MEETUP_PORT")],
  url: [host: System.get_env("CG_MEETUP_HOST"), port: System.get_env("CG_MEETUP_PORT")],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
