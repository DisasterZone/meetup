defmodule WebWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :web

  def init(_, config) do
    port = System.get_env("CG_MEETUP_PORT") || raise "expected the CG_MEETUP_PORT environment variable to be set"
    IO.puts(port)
    host = System.get_env("CG_MEETUP_HOST") || raise "expected the CG_MEETUP_HOST environment variable to be set"
    IO.puts(host)

    {:ok, Keyword.put(config, :http, [:inet6, port: port])}
    {:ok, Keyword.put(config, :url, [host: host, port: port])}
  end

  socket "/socket", WebWeb.UserSocket,
    websocket: true,
    longpoll: false

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phx.digest
  # when deploying your static files in production.
  plug Plug.Static,
    at: "/", from: :web, gzip: false,
    only: ~w(css fonts images js favicon.ico robots.txt)

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    socket "/phoenix/live_reload/socket", Phoenix.LiveReloader.Socket
    plug Phoenix.LiveReloader
    plug Phoenix.CodeReloader
  end

  plug Plug.RequestId
  plug Plug.Logger

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug Plug.MethodOverride
  plug Plug.Head

  # The session will be stored in the cookie and signed,
  # this means its contents can be read but not tampered with.
  # Set :encryption_salt if you would also like to encrypt it.
  plug Plug.Session,
    store: :cookie,
    key: "_web_key",
    signing_salt: "5qrBX0co"

  plug WebWeb.Router
end
