defmodule WebWeb.Router do
  use WebWeb, :router
  use Plug.ErrorHandler

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    # plug(PublicKeys.JwtPlug, sub: Application.get_env(:public_keys, :sub))
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", WebWeb do
    pipe_through :browser # Use the default browser stack

    resources "/users",            UserController, only: [:index, :show, :new, :create, :edit, :update]
    resources "/events",           EventController, only: [:index, :show, :new, :create, :edit, :update]
    delete    "/events/:id",       EventController, :delete
    get       "/auth",             EventController, :index
    resources "/registrations",    RegistrationController, only: [:index, :show, :create, :update, :delete]
    get       "/",                 PageController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", WebWeb do
  #   pipe_through :api
  # end

  def handle_errors(conn, %{kind: kind, reason: reason, stack: stack} = err) do
    #IO.inspect "Something happened when handling web request for the user:"
    # IO.inspect conn.assigns.current_user
    #IO.inspect "The error was:"
    #IO.inspect err

    conn
  end
end
