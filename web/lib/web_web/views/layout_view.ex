defmodule WebWeb.LayoutView do
  use WebWeb, :view

  def navbar(assigns) do
    render("navbar.html", assigns)
  end
end
