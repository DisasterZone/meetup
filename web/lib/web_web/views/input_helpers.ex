defmodule WebWeb.InputHelpers do
  use Phoenix.HTML
  use Timex

  import Aws

  def input(form, field) do
    type = Phoenix.HTML.Form.input_type(form, field)

    wrapper_opts = [class: "form-group"]
    label_opts = [class: "control-label"]
    input_opts = [class: "form-control", placeholder: humanize(field)]

    content_tag :div, wrapper_opts do
      label = label(form, field, humanize(field), label_opts)
      input = apply(Phoenix.HTML.Form, type, [form, field, input_opts])
      error = WebWeb.ErrorHelpers.error_tag(form, field) || ""
      [label, input, error]
    end
  end

  def aws_s3(url) when is_nil(url) or byte_size(url) == 0 do
    ""
  end
  def aws_s3(url) do
    img_tag(Aws.sign_url("GET", url, "us-west-1", "s3"), [class: "activator image"])
  end

  def tz_convert(datetime, office) do
    timezone = Timezone.get("America/Los_Angeles", Timex.now)
    Timezone.convert(datetime, timezone)
  end
end
