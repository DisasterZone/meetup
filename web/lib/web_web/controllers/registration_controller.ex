defmodule WebWeb.RegistrationController do
  use WebWeb, :controller

  alias Db
  alias Db.Registration

  def index(conn, _params) do
    registrations = Db.get_registrations()
    render(conn, "index.json", registrations: registrations)
  end

  def show(conn, %{"id" => id}) do
    registration = Db.get_registration_by_id(id)
    render(conn, "show.json", registration: registration)
  end

  def create(conn, %{"registration" => registration_params}) do
    with {:ok, %Registration{} = registration} <-
           Db.create_registration_waitlist(registration_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.registration_path(conn, :show, registration))
      |> render("show.json", registration: registration)
    end
  end

  def update(conn, %{"registration" => registration_params}) do
    with {:ok, %Registration{} = registration} <- Db.update_registration(registration_params) do
      render(conn, "show.json", registration: registration)
    end
  end

  def delete(conn, %{"id" => id}) do
    with {:ok, %Registration{}} <- Db.delete_registration(id) do
      send_resp(conn, :no_content, "")
    end
  end
end
