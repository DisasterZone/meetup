defmodule WebWeb.UserController do
  use WebWeb, :controller

  alias Db

  def index(conn, params) when params == %{} do
    users = Db.get_users()
    render(conn, "index.html", users: users)
  end

  def index(conn, params) do
    users = Db.get_users(params)
    render(conn, "index.html", users: users)
  end

  def show(conn, %{"id" => id}) do
    user = Db.get_user_by_id(id)
    render(conn, "show.html", user: user)
  end

  def new(conn, _params) do
    changeset = Db.change_user(%Db.User{})
    conn = conn |> assign(:locations, office_locations())

    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    # user_params = Map.put(user_params, "oauth_id", create_oauth_id(conn))

    case Db.create_user(user_params) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "#{user.first_name} created!")
        |> redirect(to: Routes.user_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def edit(conn, %{"id" => id}) do
    changeset = Db.edit_user(id)
    conn = conn |> assign(:locations, office_locations())

    render(conn, "edit.html", changeset: changeset)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Db.get_user_by_id(id)
    case Db.update_user(user, user_params) do
      {:ok, user} -> redirect(conn, to: Routes.user_path(conn, :show, user))
      {:error, user} -> render(conn, "edit.html", changeset: user)
    end
  end

  defp create_oauth_id(conn) do
    "#{conn.assigns.user_token["iss"]}:#{conn.assigns.user_token["sub"]}"
  end

  defp office_locations() do
    locations =
      [
        [key: "AMSP", value: "AMSP"],
        [key: "ATO", value: "ATO"],
        [key: "BJO", value: "BJO"],
        [key: "BJOPM", value: "BJOPM"],
        [key: "DCO", value: "DCO"],
        [key: "FTSP", value: "FTSP"],
        [key: "HRO", value: "HRO"],
        [key: "HKO", value: "HKO"],
        [key: "IND", value: "IND"],
        [key: "IRV", value: "IRV"],
        [key: "LAO", value: "LAO"],
        [key: "LAOW", value: "LAOW"],
        [key: "LBO", value: "LBO"],
        [key: "LDO", value: "LDO"],
        [key: "LXO", value: "LXO"],
        [key: "MBO", value: "MBO"],
        [key: "MDSP", value: "MDSP"],
        [key: "MLSP", value: "MLSP"],
        [key: "NYO", value: "NYO"],
        [key: "REG", value: "REG"],
        [key: "RGX", value: "RGX"],
        [key: "RNO", value: "RNO"],
        [key: "SFO", value: "SFO"],
        [key: "SIO", value: "SIO"],
        [key: "SNO", value: "SNO"],
        [key: "SPO", value: "SPO"],
        [key: "SYO", value: "SYO"],
        [key: "TKO", value: "TKO"],
        [key: "TNO", value: "TNO"],
        [key: "ZHSP", value: "ZHSP"]
      ]

    locations
  end
end
