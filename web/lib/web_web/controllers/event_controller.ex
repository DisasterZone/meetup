defmodule WebWeb.EventController do
  use WebWeb, :controller
  use Timex

  alias Db
  alias Aws
  alias Aws.Blob

  def index(conn, params) when params == %{} do
    events = Db.get_events()
    render(conn, "index.html", events: events)
  end

  def index(conn, params) do
    IO.inspect(params)
    events = Db.get_events(params)
    render(conn, "index.html", events: events)
  end

  def show(conn, %{"id" => id}) do
    event = Db.get_event_by_id(id)
    render(conn, "show.html", event: event)
  end

  def new(conn, _params) do
    changeset = Db.change_event(%Db.Event{})
    conn = conn
           |> assign(:categories, categories())
           |> assign(:stored_tags, "")

    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"event" => event_params}) do
    conn = conn
           |> assign(:categories, categories())
           |> assign(:stored_tags, "")

    {start_time, end_time} = build_start_end(event_params)

    event_params = Map.put(event_params, "start_time", start_time)
    event_params = Map.put(event_params, "end_time", end_time)
    event_params = Map.put(event_params, "tags", split_tags(event_params["tags"]))
    event_params = upload_image(event_params, event_params["image"])

    case Db.create_event(event_params) do
      {:ok, event} ->
        conn
        |> put_flash(:info, "#{event.title} created!")
        |> redirect(to: Routes.event_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def edit(conn, %{"id" => id}) do
    {event, changeset} = Db.edit_event(id)
    stored_tags = join_tags(changeset.data.tags)
    conn = conn
           |> assign(:categories, categories())
           |> assign(:stored_tags, stored_tags)

    render(conn, "edit.html", event: event, changeset: changeset)
  end

  def update(conn, %{"id" => id, "event" => event_params}) do
    event = Db.get_event_by_id(id)
    {start_time, end_time} = build_start_end(event_params)
    new_tags = split_tags(event_params["tags"])

    event_params = upload_image(event_params, event_params["image"])
    event_params = Map.put(event_params, "start_time", start_time)
    event_params = Map.put(event_params, "end_time", end_time)
    event_params = Map.put(event_params, "tags", new_tags)
    conn = conn |> assign(:categories, categories())

    case Db.update_event(event, event_params) do
      {:ok, event} -> redirect(conn, to: Routes.event_path(conn, :show, event))
      {:error, event} -> render(conn, "edit.html", changeset: event)
    end
  end

  def delete(conn, %{"id" => id}) do
    with {:ok, %Db.Event{}} <- Db.delete_event(id) do
      conn
      |> put_flash(:info, "Event deleted successfully.")
      |> redirect(to: Routes.event_path(conn, :index))
    end
  end

  defp upload_image(event_params, "data:image/png;base64," <> image_base64) do
    {:ok, image_binary} = Base.decode64(image_base64)
    filename =
      image_binary
      |> image_extension()
      |> unique_filename()

    {:ok, _} =
      Blob.put_blob(
        "meetup-profiles",
        image_binary,
        filename
      )

    Map.put(event_params,
            "image_url",
            "https://s3-us-west-1.amazonaws.com/meetup-profiles/meetup-profiles/#{filename}")
  end

  ## If not image, return blank url
  defp upload_image(event_params, _) do
    event_params
  end

  defp split_tags(tags) when tags == nil do
    ""
  end
  defp split_tags(tags) do
    String.split(tags, ",", trim: true)
  end

  defp join_tags(tags) when tags == nil do
    ""
  end
  defp join_tags(tags) do
    Enum.join(tags, ",")
  end

  # Generates a unique filename with a given extension
  defp unique_filename(extension) do
    UUID.uuid4(:hex) <> extension
  end

  # Helper functions to read the binary to determine the image extension
  defp image_extension(<<0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, _::binary>>), do: ".png"
  defp image_extension(<<0xff, 0xD8, _::binary>>), do: ".jpg"

  defp build_start_end(event_params) do
    utc_timezone = Timezone.get(:utc, Timex.now)
    user_timezone = Timezone.get("America/Los_Angeles", Timex.now)

    start_time =
      "#{event_params["start_date"]} #{event_params["start_clock"]}"
      |> Timex.parse!("{YYYY}-{M}-{D} {h24}:{0m}")
      |> Timex.to_datetime(user_timezone)
    end_time =
      "#{event_params["end_date"]} #{event_params["end_clock"]}"
      |> Timex.parse!("{M}/{D}/{YYYY} {0h12}:{0m} {AM}")
      |> Timex.to_datetime(user_timezone)

    {
      Timezone.convert(start_time, utc_timezone),
      Timezone.convert(end_time, utc_timezone)
    }
  end

  defp categories() do
    categories =
      [
        [key: "Arts & Museums", value: "1"],
        [key: "Beliefs", value: "2"],
        [key: "Book Clubs", value: "3"],
        [key: "Career & Business", value: "4"],
        [key: "Cars & Mechanics", value: "5"],
        [key: "Cooking & Baking", value: "6"],
        [key: "Family & Outings", value: "7"],
        [key: "Fashion & Beauty", value: "8"],
        [key: "Film", value: "9"],
        [key: "Foodies", value: "10"],
        [key: "Health & Wellness", value: "11"],
        [key: "Hobbies & Crafts", value: "12"],
        [key: "LGBTQ", value: "13"],
        [key: "Language & Culture", value: "14"],
        [key: "Learning", value: "15"],
        [key: "Music & Concerts", value: "16"],
        [key: "Outdoors & Adventures", value: "17"],
        [key: "Pets", value: "18"],
        [key: "Photography", value: "19"],
        [key: "Charity & Volunteerism", value: "20"],
        [key: "Sci-Fi & Games", value: "21"],
        [key: "Social", value: "22"],
        [key: "Sports & Fitness", value: "23"],
        [key: "Writing", value: "24"]
      ]
    categories
  end
end
