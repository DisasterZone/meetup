// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import css from "../css/app.scss"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import dependencies
//
import "phoenix_html"

var oktaSignIn = new OktaSignIn({
    baseUrl: "https://capgroup-dev.oktapreview.com",
    clientId: "0oab06trttuR7LU1d0h7",
    redirectUri: 'http://localhost:4200/auth',
    authParams: {
      issuer: "https://capgroup-dev.oktapreview.com/oauth2/default",
      responseType: ["token", "id_token"],
      display: "page"
    }
  });

if (oktaSignIn.token.hasTokensInUrl()) {
    oktaSignIn.token.parseTokensFromUrl(
        // If we get here, the user just logged in.
        function success(res) {
            var accessToken = res[0];
            var idToken = res[1]

            oktaSignIn.tokenManager.add("accessToken", accessToken);
            oktaSignIn.tokenManager.add("idToken", idToken);

            window.location.hash = "";
            document.getElementById("messageBox").innerHTML = "Hello, " + idToken.claims.email + "! You just logged in! :)";
            console.log('Token', oktaSignIn.tokenManager.get("accessToken"));
        },
        function error(err) {
            console.error(err);
        }
    );
} else {
    oktaSignIn.session.get(function (res) {
        // If we get here, the user is already signed in.
        if (res.status === 'ACTIVE') {
            document.getElementById("messageBox").innerHTML = "Hello, " + res.login + "! You are *still* logged in! :)";
            return;
        }

        // If we get here, the user is not logged in, so we should show the sign-in form.
        oktaSignIn.renderEl(
            { el: '#sign-in-container' },
            function success(res) {},
            function error(err) {
                console.error(err);
            }
        );
    });
}

// For the MaterialCSS mobile navbar
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {});
});

// For the MaterialCSS DatePicker
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.datepicker');
    var instances = M.Datepicker.init(elems, {format: 'mm/dd/yyyy'});
});

// For the MaterialCSS TimePicker
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.timepicker');
    var instances = M.Timepicker.init(elems, {});
});

// For the MaterialCSS Select
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems, {});
});

// For the MaterialCSS Chips/Tags
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.chips');
    var stored_tags = document.querySelector('#stored_tags');
    var chip_data = null;
    if (stored_tags != null && stored_tags.value != undefined) {
        chip_data = stored_tags.value.split(',').filter((val) => {
            return val != "";
        }).map((val) => {
            return {
                tag: val
            };
        });    
    };
    var instances = M.Chips.init(elems, {placeholder: 'Enter a tag', secondaryPlaceholder: '+Tag',
                                         data: chip_data});
});

//$('#button').click(function(){
//    alert(JSON.stringify(M.Chips.getInstance($('.chips')).chipsData));
//});

import { cropper } from "./cropper.min.js";

// Import local files
//
// Local files can be imported directly using relative paths, for example:
// import socket from "./socket"
// import "./materialize/materialize.min.js"