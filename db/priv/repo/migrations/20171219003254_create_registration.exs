defmodule Db.Repo.Migrations.CreateRegistration do
  use Ecto.Migration

  def change do
    create table(:registrations) do
      add :event_id, :integer
      add :oauth_id, :string
      add :create_datetime, :utc_datetime
      add :update_datetime, :utc_datetime
      add :waitlist_datetime, :utc_datetime
    end
  end
end