defmodule Db.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :oauth_id, :string
      add :initials, :string
      add :first_name, :string
      add :last_name, :string
      add :email, :string
      add :alt_email, :string
      add :phone, :string
      add :image_url, :string
      add :office, :string
    end

    create unique_index(:users, [:oauth_id])
  end

end