defmodule Db.Repo.Migrations.CreateEvent do
  use Ecto.Migration

  def change do
    create table(:events) do
      add :title, :string
      add :organizer, :string
      add :image_url, :string
      add :category, :string
      add :location, :string
      add :online, :boolean, null: false, default: false
      add :start_time, :utc_datetime
      add :end_time, :utc_datetime
      add :min_participants, :integer, null: false, default: 1
      add :max_participants, :integer
      add :tags, {:array, :string}
      add :description, :string
      add :active, :boolean, null: false, default: true
    end
  end

end
