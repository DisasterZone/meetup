defmodule Db.Repo do
  use Ecto.Repo, otp_app: :db

  def init(_, config) do
    #ostname = System.get_env("CG_MEETUP_DB_HOSTNAME") || raise "expected the CG_MEETUP_DB_HOSTNAME environment variable to be set"
    #{:ok, Keyword.put(config, :hostname, hostname)}

    #dbname = System.get_env("CG_MEETUP_DB_NAME") || raise "expected the CG_MEETUP_DB_NAME environment variable to be set"
    #{:ok, Keyword.put(config, :database, dbname)}

    #dbusername = System.get_env("CG_MEETUP_DB_USERNAME") || raise "expected the CG_MEETUP_DB_USERNAME environment variable to be set"
    #{:ok, Keyword.put(config, :username, dbusername)}

    dbpassword = System.get_env("CG_MEETUP_DB_PASSWORD") || raise "expected the CG_MEETUCG_MEETUP_DB_PASSWORDP_DB_USERNAME environment variable to be set"
    {:ok, Keyword.put(config, :password, dbpassword)}
  end
end
