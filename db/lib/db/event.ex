defmodule Db.Event do
  use Ecto.Schema

  alias Ecto.Changeset
  alias Db.Repo
  alias Db.Event

  import Ecto.Query, only: [from: 2]

  require Ecto.Changeset

  schema "events" do
    field(:organizer, :string)
    field(:title, :string)
    field(:image_url, :string)
    field(:category, :string)
    field(:location, :string)
    field(:online, :boolean)
    field(:start_time, :utc_datetime)
    field(:end_time, :utc_datetime)
    field(:min_participants, :integer)
    field(:max_participants, :integer)
    field(:tags, {:array, :string})
    field(:description, :string)
    field(:active, :boolean)
  end

  @required_fields ~w(organizer title)
  @optional_fields ~w(image_url category location online start_time end_time min_participants
                      max_participants description active tags)

  def create(event_params) do
    changeset = changeset(%Event{}, event_params)
    Repo.insert(changeset)
  end

  def edit(event_id) do
    event = get_event_by_id(event_id)

    {
      event,
      event
      |> Db.Event.change_event()
    }
  end

  def update(event_params) do
    event = get(event_params)
    changeset = changeset(event, event_params)
    Repo.update(changeset)
  end

  def update(%Db.Event{} = event, updates) do
    event
    |> changeset(updates)
    |> Repo.update()
  end

  def delete(event_id) do
    event = get_event_by_id(event_id)
    changeset = changeset(event, %{active: false})
    Repo.update(changeset)
  end

  def get() do
    search(%{"search_term" => "",
             "keywords" => "",
             "category" => "",
             "start_time" => "",
             "end_time" => ""})
  end

  def get(event = %Event{}) do
    get_event_by_id(event["id"])
  end

  def get(%{"id" => event_id}) do
    get_event_by_id(event_id)
  end

  def get(%{:id => event_id}) do
    get_event_by_id(event_id)
  end

  @spec get_event_by_id(any()) :: any()
  def get_event_by_id(event_id) do
    Repo.get(Event, event_id)
  end

  def search(%{"search_term" => search_term}) do
    search_params = %{"keywords" => [search_term],
                      "category" => "",
                      "start_time" => "",
                      "end_time" => ""}
    search(search_params)
  end

  def search(search_params) do
    category = search_params["category"]
    keywords = String.split(hd(search_params["keywords"]), " ", trim: true)
    start_time = search_params["start_time"]
    end_time = search_params["end_time"]

    # Ecto.Query<from e in Db.Event, or_where: like(e.tags, ^"%fun%")>
    query =
      Enum.reduce(keywords, Event, fn x, acc -> keyword(acc, x) end)
      |> category(category)
      |> start_after(start_time)
      |> end_before(end_time)
      |> title(keywords)
      |> organizer(keywords)
      |> description(keywords)
      |> active()
      # |> IO.inspect()

    Repo.all(query)
  end

  defp keyword(query, keyword) when is_nil(keyword) or byte_size(keyword) == 0 do
    query
  end

  defp keyword(query, keyword) do
    from(event in query, or_where: fragment("e0.\"tags\"::TEXT ILIKE ?", ^"%#{String.replace(keyword, "%", "\\%")}%"))
  end

  defp category(query, category) when is_nil(category) or byte_size(category) == 0 do
    query
  end

  defp category(query, category) do
    from(event in query, where: event.category == ^category)
  end

  defp start_after(query, start_time) when is_nil(start_time) or byte_size(start_time) == 0 do
    query
  end

  defp start_after(query, start_time) do
    from(event in query, where: event.start_time >= ^start_time)
  end

  defp end_before(query, end_time) when is_nil(end_time) or byte_size(end_time) == 0 do
    query
  end

  defp end_before(query, end_time) do
    from(event in query, where: event.end_time <= ^end_time)
  end

  defp title(query, keywords) when length(keywords) == 0 do
    query
  end

  defp title(query, keywords) do
    Enum.reduce(keywords, query, fn x, acc ->
      from(event in acc, or_where: fragment("e0.\"title\"::TEXT ILIKE ?", ^"%#{String.replace(x, "%", "\\%")}%"))
    end)
  end

  defp organizer(query, keywords) when length(keywords) == 0 do
    query
  end

  defp organizer(query, keywords) do
    Enum.reduce(keywords, query, fn x, acc ->
      from(event in acc, or_where: fragment("e0.\"organizer\"::TEXT ILIKE ?", ^"%#{String.replace(x, "%", "\\%")}%"))
    end)
  end

  defp description(query, keywords) when length(keywords) == 0 do
    query
  end

  defp description(query, keywords) do
    Enum.reduce(keywords, query, fn x, acc ->
      from(event in acc, or_where: fragment("e0.\"description\"::TEXT ILIKE ?", ^"%#{String.replace(x, "%", "\\%")}%"))
    end)
  end

  defp active(query) do
    from(event in query, where: event.active == true)
  end

  def change_event(%Db.Event{} = event) do
    changeset(event, %{})
  end

  defp changeset(event, params) do
    event
    |> Changeset.cast(params, @required_fields ++ @optional_fields)
    |> Changeset.validate_required([:title, :organizer])
  end
end
