defmodule Db.Registration do
  use Ecto.Schema

  alias Ecto.Changeset
  alias Db.Repo
  alias Logic

  import Ecto.Query, only: [from: 2]

  require Ecto.Changeset

  schema "registrations" do
    field(:event_id, :integer)
    field(:oauth_id, :string)
    field(:create_datetime, :utc_datetime)
    field(:update_datetime, :utc_datetime)
    field(:waitlist_datetime, :utc_datetime)
  end

  @required_fields ~w(event_id oauth_id)
  @optional_fields ~w(create_datetime update_datetime waitlist_datetime)

  def create(registration_params) do
    registration_params =
      Map.put(registration_params, "create_datetime", DateTime.utc_now())
      |> Map.put("waitlist_datetime", DateTime.utc_now())
      |> Map.delete("update_datetime")

    changeset = changeset(%Db.Registration{}, registration_params)
    Repo.insert(changeset)
  end

  def create_waitlist(registration_params) do
    # Start by creating the base record
    {:ok, struct} = create(registration_params)
    registrations = get_registrations_by_event_id(struct.event_id)
    event = Db.get_event_by_id(struct.event_id)

    {:ok, _oauth_id, eligible} =
      Logic.eligible_waitlist(registrations, event.max_participants, struct.oauth_id)

    update_from_waitlist(struct, struct.id, eligible)
  end

  defp update_from_waitlist(_struct, registration_id, true) do
    current_registration = get_registration_by_id(registration_id)
    registration_params = %{:update_datetime => DateTime.utc_now(), :waitlist_datetime => nil}
    changeset = changeset(current_registration, registration_params)
    Repo.update(changeset)
  end

  defp update_from_waitlist(struct, _registration_id, false) do
    {:ok, struct}
  end

  def update(registration_params) do
    registration = get(registration_params)
    changeset = changeset(registration, registration_params)
    Repo.update(changeset)
  end

  def delete(registration_id) do
    registration = get_registration_by_id(registration_id)
    changeset = changeset(registration)
    Repo.delete(changeset)
  end

  def get() do
    Db.Registration |> Db.Repo.all()
  end

  def get(registration = %Db.Registration{}) do
    get_registration_by_id(registration["event_id"])
  end

  def get(registration = %{}) do
    get_registration_by_id(registration["event_id"])
  end

  def get_registration_by_id(registration_id) do
    Repo.get(Db.Registration, registration_id)
  end

  def get_registrations_by_event_id(event_id) do
    query =
      from(
        r in Db.Registration,
        select:
          map(r, [
            :id,
            :event_id,
            :oauth_id,
            :create_datetime,
            :update_datetime,
            :waitlist_datetime
          ]),
        where: r.event_id == ^event_id
      )

    Repo.all(query)
  end

  defp changeset(registration, params \\ %{}) do
    registration
    |> Changeset.cast(params, @required_fields ++ @optional_fields)
    |> Changeset.validate_required([:event_id, :oauth_id])
  end
end
