defmodule Db.User do
  use Ecto.Schema

  alias Ecto.Changeset
  alias Db.Repo

  import Ecto.Query, only: [from: 2]

  require Ecto.Changeset

  schema "users" do
    field(:oauth_id, :string)
    field(:initials, :string)
    field(:first_name, :string)
    field(:last_name, :string)
    field(:email, :string)
    field(:alt_email, :string)
    field(:phone, :string)
    field(:image_url, :string)
    field(:office, :string)
  end

  @required_fields ~w(oauth_id initials first_name last_name)
  @optional_fields ~w(email alt_email phone image_url office)

  def create(user_params) do
    changeset(%Db.User{}, user_params)
    |> Repo.insert()
  end

  def edit(user_id) do
    get_user_by_id(user_id)
    |> Db.User.change_user()
  end

  def update(user_params) do
    user = get(user_params)
    user
    |> changeset(user_params)
    |> Repo.update()
  end

  def update(%Db.User{} = user, updates) do
    user
    |> changeset(updates)
    |> Repo.update()
  end

  def get() do
    Db.User |> Db.Repo.all()
  end

  def get(%{"search_term" => search_term}) do
    Db.User
    |> search(search_term)
    |> Db.Repo.all()
  end

  def get(user = %Db.User{}) do
    get_user_by_id(user["id"])
  end

  def get(user = %{}) do
    get_user_by_id(user["id"])
  end

  def get_user_by_id(user_id) do
    Repo.get(Db.User, user_id)
  end

  def get_user_by_oauth_id(oauth_id) do
    query = from(u in Db.User, where: u.oauth_id == ^oauth_id)
    Repo.all(query)
  end

  def change_user(%Db.User{} = user) do
    changeset(user, %{})
  end

  defp search(query, search_term) do
    wildcard_search = "%#{search_term}%"

    from user in query,
    where: ilike(user.initials, ^wildcard_search) or
           ilike(user.last_name, ^wildcard_search) or
           ilike(user.first_name, ^wildcard_search)
  end

  defp changeset(user, params) do
    user
    |> Changeset.cast(params, @required_fields ++ @optional_fields)
    |> Changeset.validate_required([:oauth_id, :initials, :first_name, :last_name])
    |> Changeset.unique_constraint(:oauth_id)
  end
end
