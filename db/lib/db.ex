defmodule Db do
  defdelegate create_user(user_params), to: Db.User, as: :create
  defdelegate edit_user(user_id), to: Db.User, as: :edit
  defdelegate update_user(user, updates), to: Db.User, as: :update
  defdelegate update_user(user_params), to: Db.User, as: :update
  defdelegate get_users(), to: Db.User, as: :get
  defdelegate get_users(search_term), to: Db.User, as: :get
  defdelegate get_user_by_id(user_id), to: Db.User
  defdelegate get_user_by_oauth_id(oauth_id), to: Db.User
  defdelegate change_user(user_params), to: Db.User

  defdelegate create_event(event_params), to: Db.Event, as: :create
  defdelegate edit_event(event_id), to: Db.Event, as: :edit
  defdelegate update_event(event, updates), to: Db.Event, as: :update
  defdelegate update_event(event_params), to: Db.Event, as: :update
  defdelegate delete_event(event_id), to: Db.Event, as: :delete
  defdelegate get_events(), to: Db.Event, as: :get
  defdelegate get_events(search_terms), to: Db.Event, as: :search
  defdelegate get_event_by_id(event_id), to: Db.Event
  defdelegate change_event(event_params), to: Db.Event

  defdelegate search(search_params), to: Db.Event

  defdelegate create_registration_waitlist(registration_params),
    to: Db.Registration,
    as: :create_waitlist

  defdelegate update_registration(registration_params), to: Db.Registration, as: :update
  defdelegate delete_registration(registration_id), to: Db.Registration, as: :delete
  defdelegate get_registrations(), to: Db.Registration, as: :get
  defdelegate get_registration_by_id(registration_id), to: Db.Registration
end
