import React from 'react';
import { Paper, Tab, Tabs }  from '@material-ui/core/';

export default ({ eventCategories }) =>
  <Paper>
    <Tabs
      value={0}
      indicatorColor="primary"
      textColor="primary"
      centered
    >
      <Tab label="All" />
      {eventCategories.map(category =>
        <Tab label={category} />
      )}
    </Tabs>
  </Paper>