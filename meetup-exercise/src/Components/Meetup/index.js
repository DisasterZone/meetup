import React, { fragment } from 'react';
import { Grid, List, ListItem, ListItemText, Paper, Typography } from '@material-ui/core';

const styles = {
    Paper: {
        padding: 20,
        marginTop: 10,
        marginBottom: 10,
        height: 500,
        overflowY: 'auto'
    }
}

export default ({ events }) =>
    <Grid container>
        <Grid item sm>
            <Paper style={styles.Paper}>
                {events.map(([group, events]) =>
                    <fragment>
                        <Typography
                            variant="headline"
                            style={{textTransform: 'capitalize'}}
                        >
                            {group}
                        </Typography>
                        <List component="ul">
                            {events.map(({ title }) =>
                                <ListItem button>
                                    <ListItemText primary={title} />
                                </ListItem>
                            )}
                        </List>
                    </fragment>
                )}
            </Paper>
        </Grid>
        <Grid item sm>
            <Paper style={styles.Paper}>
                <Typography
                    variant="display1"
                >
                    Welcome!
                </Typography>
                <Typography
                    variant="subheading"
                    style={{marginTop: 20}}
                >
                    Please select an event from the list on the left.                    
                </Typography>
            </Paper>
        </Grid>
    </Grid>