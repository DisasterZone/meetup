import React, { Component, Fragment } from 'react';
import { Header, Footer } from './Layouts';
import Meetup from './Meetup';
import { eventCategories, categoryAll, officeLocations, events } from '../store.js';

export default class extends Component {
  states = {
    events
  }

  getEventsByCategory() {
    return Object.entries(
      this.states.events.reduce((events, event) => {
        const { category } = event;

        events[category] = events[category]
          ? [...events[category], event]
          : [event];

        return events;
      }, {})
    )
  }

  render() {
    const events = this.getEventsByCategory();

    return <Fragment>
      <Header/>
      <Meetup
        events={events}
      />
      <Footer
        eventCategories={eventCategories}
      />
    </Fragment>
  }
}