export const eventCategories = [
    "Arts & Museums",
    "Beliefs",
    "Book Clubs",
    "Career & Business",
    "Cars & Mechanics",
    "Cooking & Baking",
    "Family & Outings",
    "Fashion & Beauty",
    "Film",
    "Foodies",
    "Health & Wellness",
    "Hobbies & Crafts",
    "LGBTQ",
    "Language & Culture",
    "Learning",
    "Music & Concerts",
    "Outdoors & Adventures",
    "Pets",
    "Photography",
    "Charity & Volunteerism",
    "Sci-Fi & Games",
    "Social",
    "Sports & Fitness",
    "Writing"
];

export const categoryAll = 'All';

export const officeLocations = [
    "AMSP",
    "ATO",
    "BJO",
    "BJOPM",
    "CHO",
    "DCO",
    "FTSP",
    "GVO",
    "HKO",
    "HRO",
    "IND",
    "IRV",
    "LAO",
    "LAOW",
    "LBO",
    "LDO",
    "LXO",
    "MBO",
    "MDSP",
    "MLSP",
    "NYO",
    "REG",
    "RGX",
    "RNO",
    "SFO",
    "SIO",
    "SNO",
    "SPO",
    "SYO",
    "TKO",
    "TNO",
    "ZHSP"
];

export const events = [
    {
        "title": "First",
        "tags": [
            "fun",
            "first"
        ],
        "startTime": "2018-08-19T18:00:00.000000Z",
        "organizer": "CGL",
        "online": false,
        "minParticipants": 1,
        "maxParticipants": 3,
        "location": "IRV",
        "imageUrl": null,
        "id": 1,
        "endTime": "2018-08-19T20:00:00.000000Z",
        "description": "Fun",
        "category": "Music",
        "active": true
    },
    {
        "title": "First",
        "tags": [
            "fun",
            "first"
        ],
        "startTime": "2018-08-19T18:00:00.000000Z",
        "organizer": "CGL",
        "online": false,
        "minParticipants": 1,
        "maxParticipants": 3,
        "location": "IRV",
        "imageUrl": null,
        "id": 2,
        "endTime": "2018-08-19T20:00:00.000000Z",
        "description": "Fun",
        "category": "Concerts",
        "active": true
    },
    {
        "title": "First",
        "tags": [
            "fun",
            "first"
        ],
        "startTime": "2018-08-19T18:00:00.000000Z",
        "organizer": "CGL",
        "online": false,
        "minParticipants": 1,
        "maxParticipants": 3,
        "location": "IRV",
        "imageUrl": null,
        "id": 3,
        "endTime": "2018-08-19T20:00:00.000000Z",
        "description": "Fun",
        "category": "M & C",
        "active": true
    },
    {
        "title": "First",
        "tags": [
            "fun",
            "first"
        ],
        "startTime": "2018-08-19T18:00:00.000000Z",
        "organizer": "CGL",
        "online": false,
        "minParticipants": 1,
        "maxParticipants": 3,
        "location": "IRV",
        "imageUrl": null,
        "id": 4,
        "endTime": "2018-08-19T20:00:00.000000Z",
        "description": "Fun",
        "category": "Music & Concerts",
        "active": true
    },
    {
        "title": "First",
        "tags": [
            "fun",
            "first"
        ],
        "startTime": "2018-08-19T18:00:00.000000Z",
        "organizer": "CGL",
        "online": false,
        "minParticipants": 1,
        "maxParticipants": 3,
        "location": "IRV",
        "imageUrl": "https://s3-us-west-2.amazonaws.com/cgmeetup-profiles/cgmeetup-profiles.s3.amazonaws.com/dfd6bd236a19453fb3243e9188e3e156.jpg",
        "id": 5,
        "endTime": "2018-08-19T20:00:00.000000Z",
        "description": "Fun",
        "category": "Magic",
        "active": true
    },
    {
        "title": "Islands",
        "tags": [
            "islands",
            "sun",
            "surf"
        ],
        "startTime": "2018-08-26T15:00:00.000000Z",
        "organizer": "Chris Luff",
        "online": false,
        "minParticipants": 1,
        "maxParticipants": 2,
        "location": "Hawaii",
        "imageUrl": "https://s3-us-west-2.amazonaws.com/cgmeetup-profiles/cgmeetup-profiles.s3.amazonaws.com/3596f0f472b34d47acb57b787302f161.jpg",
        "id": 6,
        "endTime": "2018-09-30T18:00:00.000000Z",
        "description": "The good life",
        "category": "Sports & Fitness",
        "active": true
    }
];