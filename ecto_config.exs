# Repo configuration for Store & Phoenix Apps
config :shared, :db_config,
  adapter:  Ecto.Adapters.Postgres,
  database: "cg_meetup",
  username: "postgres",
  password: "postgres",
  hostname: "localhost"