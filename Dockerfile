FROM bitwalker/alpine-elixir:latest as build

COPY . /app

WORKDIR /app/web-api

ENV MIX_ENV=prod
ENV REPLACE_OS_VARS=true

RUN apk update && apk add --no-cache build-base

RUN mix deps.get
RUN mix compile
RUN mix release --env=prod

RUN APP_NAME="meetup_api" && \
    RELEASE_DIR="/app/web-api/_build/prod/rel/$APP_NAME/releases/0.0.1" && \
    mkdir /export && \
    tar -xf "$RELEASE_DIR/$APP_NAME.tar.gz" -C /export


#FROM pentacent/alpine-erlang-base:latest
#FROM bitwalker/alpine-erlang:latest
#FROM erlang:latest
FROM bitwalker/alpine-elixir:latest
USER default


EXPOSE 4000
ENV REPLACE_OS_VARS=true \
    PORT=4000
ENV CG_MEETUP_DB_USERNAME postgres
ENV CG_MEETUP_DB_PASSWORD password
ENV CG_MEETUP_DB_HOSTNAME 10.3.70.225

COPY --from=build /export/ .
#RUN find / -name "tzdata-0.5.14" -print
#RUN ls -l /opt/app/lib/tzdata-0.5.14/priv
#RUN chmod 777 /opt/app/lib/tzdata-0.5.14/priv

ENTRYPOINT ["/opt/app/bin/meetup_api", "foreground"]