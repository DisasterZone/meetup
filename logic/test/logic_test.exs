defmodule LogicTest do
  use ExUnit.Case
  doctest Logic

  alias Logic

  defp create_registrations do
    registrations = [
      %{
        id: 1,
        event_id: 1,
        oauth_id: "cguser:1234",
        create_datetime: DateTime.utc_now(),
        update_datetime: DateTime.utc_now(),
        waitlist_datetime: nil
      },
      %{
        id: 2,
        event_id: 1,
        oauth_id: "cguser:5678",
        create_datetime: DateTime.utc_now(),
        update_datetime: nil,
        waitlist_datetime: nil
      },
      %{
        id: 3,
        event_id: 1,
        oauth_id: "cguser:9876",
        create_datetime: DateTime.utc_now(),
        update_datetime: nil,
        waitlist_datetime: DateTime.utc_now()
      },
      %{
        id: 4,
        event_id: 1,
        oauth_id: "cguser:5432",
        create_datetime: DateTime.utc_now(),
        update_datetime: nil,
        waitlist_datetime: DateTime.utc_now()
      },
      %{
        id: 5,
        event_id: 1,
        oauth_id: "cguser:1357",
        create_datetime: DateTime.utc_now(),
        update_datetime: nil,
        waitlist_datetime: DateTime.utc_now()
      }
    ]
  end

  test "eligible to come off waitlist" do
    registrations = create_registrations()

    assert {:ok, "cguser:5678", false} == Logic.eligible_waitlist(registrations, 2, "cguser:5678")
    assert {:ok, "cguser:9876", false} == Logic.eligible_waitlist(registrations, 2, "cguser:9876")
    assert {:ok, "cguser:9876", true} == Logic.eligible_waitlist(registrations, 3, "cguser:9876")
    assert {:ok, "cguser:5432", false} == Logic.eligible_waitlist(registrations, 3, "cguser:5432")
    assert {:ok, "cguser:1357", false} == Logic.eligible_waitlist(registrations, 4, "cguser:1357")
    assert {:ok, "cguser:1357", true} == Logic.eligible_waitlist(registrations, 5, "cguser:1357")
  end
end
