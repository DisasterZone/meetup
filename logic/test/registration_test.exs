defmodule RegistrationTest do
  use ExUnit.Case
  doctest Logic

  alias Logic.Registration

  defp create_registrations do
    registrations = [
      %{
        id: 1,
        event_id: 1,
        oauth_id: "cguser:1234",
        create_datetime: DateTime.utc_now(),
        update_datetime: DateTime.utc_now(),
        waitlist_datetime: nil
      },
      %{
        id: 2,
        event_id: 2,
        oauth_id: "cguser:5678",
        create_datetime: DateTime.utc_now(),
        update_datetime: nil,
        waitlist_datetime: nil
      },
      %{
        id: 3,
        event_id: 3,
        oauth_id: "cguser:9876",
        create_datetime: DateTime.utc_now(),
        update_datetime: nil,
        waitlist_datetime: DateTime.utc_now()
      },
      %{
        id: 4,
        event_id: 4,
        oauth_id: "cguser:5432",
        create_datetime: DateTime.utc_now(),
        update_datetime: nil,
        waitlist_datetime: DateTime.utc_now()
      },
      %{
        id: 5,
        event_id: 5,
        oauth_id: "cguser:1357",
        create_datetime: DateTime.utc_now(),
        update_datetime: nil,
        waitlist_datetime: DateTime.utc_now()
      }
    ]
  end

  test "is active user in registration" do
    registrations = create_registrations()

    assert {true, "cguser:1234"} == Registration.is_active(registrations, "cguser:1234")
    assert {false, "cguser:9876"} == Registration.is_active(registrations, "cguser:9876")
  end

  test "count number of active participants" do
    registrations = create_registrations()

    assert 2 == Registration.participant_count(registrations)
  end

  test "the number of waitlist members" do
    registrations = create_registrations()

    assert 3 == Registration.waitlist_count(registrations)
  end

  test "determine waitlist slot position" do
    registrations = create_registrations()

    assert 0 == Registration.waitlist_slot(registrations, "cguser:9876")
    assert 1 == Registration.waitlist_slot(registrations, "cguser:5432")
    assert 2 == Registration.waitlist_slot(registrations, "cguser:1357")
  end

  test "check for eligibility to come off waitlist" do
    registrations = create_registrations()

    assert {false, "cguser:9876"} ==
             Registration.eligible_off_waitlist(registrations, 2, "cguser:9876")

    assert {false, "cguser:1357"} ==
             Registration.eligible_off_waitlist(registrations, 2, "cguser:1357")

    assert {true, "cguser:9876"} ==
             Registration.eligible_off_waitlist(registrations, 3, "cguser:9876")

    assert {false, "cguser:5432"} ==
             Registration.eligible_off_waitlist(registrations, 3, "cguser:5432")

    assert {true, "cguser:5432"} ==
             Registration.eligible_off_waitlist(registrations, 4, "cguser:5432")

    assert {false, "cguser:1357"} ==
             Registration.eligible_off_waitlist(registrations, 4, "cguser:1357")

    assert {true, "cguser:1357"} ==
             Registration.eligible_off_waitlist(registrations, 5, "cguser:1357")
  end
end
