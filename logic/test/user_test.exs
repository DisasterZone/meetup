defmodule UserTest do
  use ExUnit.Case
  doctest Logic

  alias Logic.User

  defp create_token_map do
    token_map = %{
      "amr" => ["pwd"],
      "at_hash" => "yTfknKoYzv7dv8KhQB_h8g",
      "aud" => "0oab06trttuR7LU1d0h7",
      "auth_time" => 1_515_037_798,
      "exp" => 1_515_041_398,
      "iat" => 1_515_037_798,
      "idp" => "00o2m98xlpRWCXKWSCAP",
      "iss" => "https://capgroup-dev.oktapreview.com",
      "jti" => "ID.bBPhj0FPHzXqkOfG4Mo-rDPaDSjquPurwR2qivzRWIQ",
      "name" => "Chris Luff (CGL)",
      "nonce" => "1309040bfeee4661917c718d4dc8ef17",
      "preferred_username" => "CGL@cguser.capgroup.com",
      "sub" => "00u2pjztvzRGCYNYNBHY",
      "ver" => 1
    }
  end

  test "parsing initials out of string" do
    name = "Chris Gordon Luff (CGL)"
    tokens = String.split(name, " ")

    assert "CGL" == User.parse_initials(tokens)
  end

  test "parsing last_name out of string" do
    name = "Chris Gordon Luff (CGL)"
    tokens = String.split(name, " ")

    assert "Luff" == User.parse_last_name(tokens)
  end

  test "parsing first_name out of string" do
    name = "Chris Gordon Luff (CGL)"
    tokens = String.split(name, " ")

    assert "Chris Gordon" == User.parse_first_name(tokens)
  end
end
