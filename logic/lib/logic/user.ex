defmodule Logic.User do
  def parse_name(name) do
    tokens = String.split(name, " ")

    %{
      "initials" => parse_initials(tokens),
      "last_name" => parse_last_name(tokens),
      "first_name" => parse_first_name(tokens)
    }
  end

  def parse_initials(tokens) do
    Enum.take(tokens, -1) |> Enum.at(0) |> String.trim("(") |> String.trim(")")
  end

  def parse_last_name(tokens) do
    Enum.take(tokens, -2) |> Enum.at(0)
  end

  def parse_first_name(tokens) do
    count = Enum.count(tokens)
    Enum.take(tokens, count - 2) |> Enum.join(" ")
  end
end
