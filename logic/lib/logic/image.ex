defmodule Logic.Image do
  def upload_image(id, image_base64) do
    {:ok, image_binary} = Base.decode64(image_base64)

    filename =
      image_binary
      |> image_extension()
      |> unique_filename(id)

    filename
  end

  defp unique_filename(extension, id) do
    id <> extension
  end

  defp image_extension(<<0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, _::binary>>), do: ".png"
  defp image_extension(<<0xFF, 0xD8, _::binary>>), do: ".jpg"
end
