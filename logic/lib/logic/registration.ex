defmodule Logic.Registration do
  ## Take a list of registrations and determine whether
  ## a specific user (as referred by oauth_id) is eligible
  ## to be moved into the participant bucket
  def eligible_waitlist(registrations, max_participants, oauth_id) do
    with {active, _} <- is_active(registrations, oauth_id),
         {eligible, _} <- eligible_off_waitlist(registrations, max_participants, oauth_id) do
      {:ok, oauth_id, eligible}
    end

    # |> eligible_off_waitlist(3, oauth_id)
    # 1) if already a participant, quick quit {:done, "already a participant"}
    # 2) if max_participants - num_current_participants > slot_in_waitlist, {:ok, "allowed to participate"}
    # 3) otherwise {:no, "not enough slots currently"}
    # 4) not on list at all {:error, "not a participant"}
  end

  def is_active(registrations, oauth_id) do
    result =
      Enum.find(registrations, :no, fn x ->
        x[:oauth_id] == oauth_id and x[:waitlist_datetime] == nil
      end)

    case result do
      :no -> {false, oauth_id}
      _ -> {true, result[:oauth_id]}
    end
  end

  def eligible_off_waitlist(registrations, max_participants, oauth_id) do
    available_slots = max_participants - participant_count(registrations)

    case available_slots > waitlist_slot(registrations, oauth_id) do
      false -> {false, oauth_id}
      _ -> {true, oauth_id}
    end
  end

  def participant_count(registrations) do
    Enum.count(registrations, fn x -> x[:waitlist_datetime] == nil end)
  end

  def waitlist_count(registrations) do
    Enum.count(registrations, fn x -> x[:waitlist_datetime] != nil end)
  end

  def waitlist_slot(registrations, oauth_id) do
    Enum.filter(registrations, fn x -> x[:waitlist_datetime] != nil end)
    |> Enum.find_index(fn x -> x[:oauth_id] == oauth_id end)
  end
end
