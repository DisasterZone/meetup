defmodule Logic do
  alias Logic.Registration

  defdelegate eligible_waitlist(registrations, max_participants, oauth_id), to: Registration
  # def eligible_waitlist(pid, registrations, oauth_id) do
  #  GenServer.call(pid, { :eligible_waitlist, registrations, oauth_id })
  # end
end
