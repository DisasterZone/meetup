defmodule MeetupAPI.Mixfile do
  use Mix.Project

  def project do
    [
      app: :meetup_api,
      version: "0.0.1",
      elixir: "~> 1.4",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {MeetupAPI.Application, []},
      # , :jsx, :ex_azure],
      extra_applications: [:logger, :wobserver, :runtime_tools, :phoenix_ecto, :db]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:distillery, "~> 2.0-rc.12", override: true},
      {:wobserver, "~> 0.1.8"},
      {:mix_docker, "~> 0.5.0"},
      {:phoenix, "~> 1.3.0"},
      {:phoenix_ecto, "~> 3.3.0"},
      {:phoenix_pubsub, "~> 1.0"},
      {:gettext, "~> 0.11"},
      {:cowboy, "~> 1.1"},
      {:phoenix_swagger, "~> 0.7.0"},
      {:ex_json_schema, "~> 0.5"},
      {:cors_plug, "~> 1.2"},
      {:uuid, "~> 1.1"},
      {:proper_case, git: "git://github.com/Disaster-Zone/proper_case.git"},
      {:azure, [path: "../azure"]},
      {:aws, [path: "../aws"]},
      {:db, [path: "../db"]},
      {:logic, [path: "../logic"]},
      {:public_keys, [path: "../public_keys"]},
    ]
  end

  defp aliases do
    [
      # , "run priv/repo/seeds.exs"],
      "ecto.setup": ["ecto.create", "ecto.migrate"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      swagger: ["phx.swagger.generate priv/static/swagger.json"]
    ]
  end
end
