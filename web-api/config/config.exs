# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

config :db, ecto_repos: [Db.Repo]

config :db, Db.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "meetup",
  username: "meetup", # meetup, user
  password: "prs-1234", # prs-1234, prs123
  hostname: "meetup.cimecrch6cy3.us-east-2.rds.amazonaws.com" # meetup.cimecrch6cy3.us-east-2.rds.amazonaws.com, localhost

# General application configuration
config :meetup_api, namespace: MeetupAPI

# Configures the endpoint
config :meetup_api, MeetupAPIWeb.Endpoint,
  url: [host: "localhost"],  # [ip: {0,0,0,0}],
  secret_key_base: "xd0HWTMpj0hhKYZX7jnX+Dwc9tCijgMr0NKJoOSUK0LrZAdBqjk4jQItM9HE/ic0",
  render_errors: [view: MeetupAPIWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: MeetupAPI.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :wobserver,
  mode: :plug,
  remote_url_prefix: "/wobserver"

config :ex_aws,
  access_key_id:     System.get_env("CG_MEETUP_AWS_ACCESS_KEY_ID"),
  secret_access_key: System.get_env("CG_MEETUP_AWS_SECRET_ACCESS_KEY"),
  s3: [
    scheme: "https://",
    host: "cgmeetup-profiles.s3.amazonaws.com",
    region: "us-west-2"
  ]

config :azure,
  storage_service_version: "2017-04-17",
  account: System.get_env("CG_MEETUP_AZURE_ACCOUNT"),
  access_key: System.get_env("CG_MEETUP_AZURE_ACCESS_KEY")

# config :web,
#  ecto_repos: [Db.Repo]

# This identifies the application sub which will
# be authorized
config :public_keys, sub: "00u2pjztvzRGCYNYNBHY"
config :public_keys, pk_url: "https://capgroup-dev.oktapreview.com/oauth2/v1/keys"

config :phoenix, :generators,
  migration: false,
  schema: false

config :phoenix, :format_encoders, json: ProperCase.JSONEncoder.CamelCase
config :phoenix, :format_encoders, json: MeetupAPIWeb.JsonEncoder

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
