# MeetupAPI

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Start Phoenix endpoint with `mix phx.server`
  * Mix.Tasks.Phx.Routes.run ''
  * source .env; elixir --detached -S mix phx.server
  * sudo netstat -plnt

  * ssh -i "meetup.pem" ubuntu@ec2-54-202-15-110.us-west-2.compute.amazonaws.com

  *  1  wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb
  *  2  sudo dpkg -i erlang-solutions_1.0_all.deb
  *  3  sudo apt-get update
  *  4  sudo apt-get install esl-erlang
  *  5  sudo apt-get install elixir
  *  6  mix local.hex
  *  7  mix archive.install https://github.com/phoenixframework/archives/raw/master/1.4-dev/phx_new.ez

  * git fetch origin master
  * git reset --hard FETCH_HEAD
  * git clean -df

  * ng serve --host 0.0.0.0 --public-host ec2-18-188-151-54.us-east-2.compute.amazonaws.com --prod
  * nohup ng serve --host 0.0.0.0 --public-host ec2-18-188-151-54.us-east-2.compute.amazonaws.com --prod 2>&1 >> ./ng.log &

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
