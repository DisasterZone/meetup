defmodule MeetupAPIWeb.Router do
  use MeetupAPIWeb, :router

  # import PublicKeys.JwtPlug

  pipeline :api do
    plug CORSPlug, origin: ["http://localhost:4200", "ec2-18-188-151-54.us-east-2.compute.amazonaws.com:4200"]
    plug(:accepts, ["json"])
    # plug(PublicKeys.JwtPlug, sub: Application.get_env(:public_keys, :sub))
    plug(ProperCase.Plug.SnakeCaseParams)
  end

  scope "/swagger" do
    forward(
      "/",
      PhoenixSwagger.Plug.SwaggerUI,
      otp_app: :meetup_api,
      swagger_file: "swagger.json",
      disable_validator: true
    )
  end

  scope "/wobserver" do
    forward("/", Wobserver.Web.Router)
  end

  scope "/api", MeetupAPIWeb do
    pipe_through([:api])

    get("/profile/:id", UserController, :show)
    get("/profiles", UserController, :index)
    put("/profile", UserController, :update)
    post("/profile", UserController, :create)
    post("/login", UserController, :create_default)
    get("/profile", UserController, :show_self)

    get("/event/:id", EventController, :show)
    get("/events", EventController, :index)
    put("/event", EventController, :update)
    post("/event", EventController, :create)
    delete("/event/:id", EventController, :delete)

    post("/search", EventController, :search)

    get("/event/registration/:id", RegistrationController, :show)
    get("/event/registrations", RegistrationController, :index)
    put("/event/registration", RegistrationController, :update)
    post("/event/registration", RegistrationController, :create)

    post("/images", ImageController, :create)
  end

  def swagger_info do
    %{
      info: %{
        version: "0.0.1",
        title: "Meetup API"
      }
    }
  end
end
