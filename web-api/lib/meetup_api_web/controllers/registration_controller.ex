defmodule MeetupAPIWeb.RegistrationController do
  use MeetupAPIWeb, :controller
  use PhoenixSwagger

  alias Db
  alias Db.Registration

  action_fallback(MeetupAPIWeb.FallbackController)

  def swagger_definitions do
    %{
      Registration:
        swagger_schema do
          title("Registration")
          description("An user registered for an event")

          properties do
            event_id(:integer, "The event ID", required: true)
            oauth_id(:string, "The unique ID of User", required: true)
            create_datetime(:string, "When the registration was created", format: "ISO-8601")
            update_datetime(:string, "When the registration was updated", format: "ISO-8601")

            waitlist_datetime(
              :string,
              "When the registration was added to waitlist",
              format: "ISO-8601"
            )
          end

          example(%{
            event_id: 1,
            oauth_id: "https://capgroup-dev.oktapreview.com:00u2pjztvzRGCYNYNBHY",
            create_datetime: "2018-01-02T20:50:00.000000Z",
            update_datetime: "2018-01-02T20:50:00.000000Z",
            waitlist_datetime: "2018-01-02T20:50:00.000000Z"
          })
        end,
      Registrations:
        swagger_schema do
          title("Registrations")
          description("All registrations")
          type(:array)
          items(Schema.ref(:Registration))
        end,
      Error:
        swagger_schema do
          title("Errors")
          description("Error responses from the API")

          properties do
            error(:string, "The message of the error raised", required: true)
          end
        end
    }
  end

  swagger_path(:index) do
    get("/api/event/registrations")
    summary("List all Registrations")
    description("List all Registrations")
    response(200, "Ok", Schema.ref(:Registration))
  end

  def index(conn, _params) do
    registrations = Db.get_registrations()
    render(conn, "index.json", registrations: registrations)
  end

  swagger_path(:show) do
    get("/api/event/registration/{id}")
    summary("Retrieve a registration")
    description("Retrieve a registration that has been saved")

    parameters do
      id(:path, :integer, "The id of the registration", required: true)
    end

    response(200, "Ok", Schema.ref(:Registration))
    response(404, "Not found", Schema.ref(:Error))
  end

  def show(conn, %{"id" => id}) do
    registration = Db.get_registration_by_id(id)
    render(conn, "show.json", registration: registration)
  end

  swagger_path(:create) do
    post("/api/event/registration/")
    summary("Add a new registration")
    description("Record a new registration request")

    parameters do
      registration(:body, Schema.ref(:Registration), "Registration to record", required: true)
    end

    response(201, "Ok", Schema.ref(:Registration))
    response(422, "Unprocessable Entity", Schema.ref(:Error))
  end

  def create(conn, %{"registration" => registration_params}) do
    with {:ok, %Registration{} = registration} <-
           Db.create_registration_waitlist(registration_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", registration_path(conn, :show, registration))
      |> render("show.json", registration: registration)
    end
  end

  swagger_path(:update) do
    put("/api/event/registration/")
    summary("Update an existing registration")
    description("Update an existing registration")

    parameters do
      registration(:body, Schema.ref(:Registration), "Registration with updates", required: true)
    end

    response(201, "Ok", Schema.ref(:Registration))
    response(422, "Unprocessable Entity", Schema.ref(:Error))
  end

  def update(conn, %{"registration" => registration_params}) do
    with {:ok, %Registration{} = registration} <- Db.update_registration(registration_params) do
      render(conn, "show.json", registration: registration)
    end
  end

  swagger_path :delete do
    delete("/api/event/registration/{id}")
    summary("Delete a registration by id")
    description("Delete a registration by id")

    parameters do
      id(:path, :integer, "The id of the registration", required: true)
    end

    response(204, "No content")
  end

  def delete(conn, %{"id" => id}) do
    with {:ok, %Registration{}} <- Db.delete_registration(id) do
      send_resp(conn, :no_content, "")
    end
  end
end
