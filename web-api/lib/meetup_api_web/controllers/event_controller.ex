defmodule MeetupAPIWeb.EventController do
  use MeetupAPIWeb, :controller
  use PhoenixSwagger

  alias Db
  alias Db.Event
  alias Aws
  alias Aws.Blob

  action_fallback(MeetupAPIWeb.FallbackController)

  def swagger_definitions do
    %{
      Event:
        swagger_schema do
          title("Event")
          description("An event definition")

          properties do
            id(:integer, "The event ID", required: true)
            organizer(:string, "Organizer of the event", required: true)
            title(:string, "The title of an event", required: true)
            image_url(:string, "Image representing the event")
            category(:string, "Category of the event")
            location(:string, "Location of the event")
            online(:boolean, "Is this online?")
            start_time(:string, "When the event starts", format: "ISO-8601")
            end_time(:string, "When the event finishes", format: "ISO-8601")
            min_participants(:integer, "Minimum attendees")
            max_participants(:integer, "Maximum attendees")
            tags(:string, "Tags to indicate interests, etc")
            description(:string, "Description of the event")
            active(:boolean, "Is the event still currently active?")
          end

          example(%{
            id: 1,
            organizer: "CGL",
            title: "Games with AA",
            category: "Games",
            location: "IRV Central Green",
            online: false,
            start_time: "2018-01-02T20:50:00Z",
            end_time: "2018-01-02T22:00:00Z",
            min_participants: 1,
            max_participants: 10,
            tags: "fun, grass, balls",
            description: "No words can explain",
            active: true
          })
        end,
      Events:
        swagger_schema do
          title("Events")
          description("All events")
          type(:array)
          items(Schema.ref(:Event))
        end,
      Error:
        swagger_schema do
          title("Errors")
          description("Error responses from the API")

          properties do
            error(:string, "The message of the error raised", required: true)
          end
        end
    }
  end

  swagger_path(:index) do
    get("/api/events")
    summary("List all events")
    description("List all events")
    response(200, "Ok", Schema.ref(:Events))
  end

  def index(conn, _params) do
    events = Db.get_events()
    render(conn, "index.json", events: events)
  end

  swagger_path(:show) do
    get("/api/event/{id}")
    summary("Retrieve an event")
    description("Retrieve an event that has been saved")

    parameters do
      id(:path, :integer, "The id of the event", required: true)
    end

    response(200, "Ok", Schema.ref(:Event))
    response(404, "Not found", Schema.ref(:Error))
  end

  def show(conn, %{"id" => id}) do
    event = Db.get_event_by_id(id)
    render(conn, "show.json", event: event)
  end

  swagger_path(:create) do
    post("/api/event/")
    summary("Add a new event")
    description("Record a new event")

    parameters do
      event(:body, Schema.ref(:Event), "Event information", required: true)
    end

    response(201, "Ok", Schema.ref(:Event))
    response(422, "Unprocessable Entity", Schema.ref(:Error))
  end

  def create(conn, %{"file" => file_upload, "meetup_event" => event_params}) do
    event_params = Poison.decode!(event_params) |> ProperCase.to_snake_case()
    # Temporary hardcode since I'm not certain the UI integration rules
    event_params = Map.put(event_params, "active", true)
    event_params = Map.put(event_params, "online", false)

    with {:ok, %Event{} = event} <- Db.create_event(event_params) do
      {:ok, image_file} = File.read(file_upload.path)
      file_uuid = UUID.uuid4(:hex)

      {:ok, _} =
        Blob.put_blob(
          "cgmeetup-profiles.s3.amazonaws.com",
          image_file,
          "#{file_uuid}.#{image_type(file_upload.content_type)}"
        )

      image_url = "https://s3-us-west-2.amazonaws.com/cgmeetup-profiles/cgmeetup-profiles.s3.amazonaws.com/#{file_uuid}.#{image_type(file_upload.content_type)}"
      event_params = %{Map.from_struct(event) | image_url: image_url}

      with {:ok, %Event{} = event} <- Db.update_event(event_params) do
        conn
        |> put_status(:created)
        |> put_resp_header("location", event_path(conn, :show, event))

        render(conn, "show.json", event: event)
      end
    end
  end

  def create(conn, %{"meetup_event" => event_params}) do
    event_params = Poison.decode!(event_params) |> ProperCase.to_snake_case()

    with {:ok, %Event{} = event} <- Db.create_event(event_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", event_path(conn, :show, event))
      |> render("show.json", event: event)
    end
  end

  swagger_path(:update) do
    put("/api/event/")
    summary("Update an existing event")
    description("Update an existing event")

    parameters do
      event(:body, Schema.ref(:Event), "Event with updates", required: true)
    end

    response(201, "Ok", Schema.ref(:Event))
    response(422, "Unprocessable Entity", Schema.ref(:Error))
  end

  def update(conn, %{"file" => file_upload, "meetup_event" => event_params}) do
    event_params = Poison.decode!(event_params)

    {:ok, image_file} = File.read(file_upload.path)
    file_uuid = UUID.uuid4(:hex)

    {:ok, _} =
      Blob.put_blob(
        "cgmeetup-profiles.s3.amazonaws.com",
        image_file,
        "#{file_uuid}.#{image_type(file_upload.content_type)}"
      )

    image_url = "https://s3-us-west-2.amazonaws.com/cgmeetup-profiles/cgmeetup-profiles.s3.amazonaws.com/#{file_uuid}.#{image_type(file_upload.content_type)}"
    event_params = Map.put(event_params, "image_url", image_url)

    with {:ok, %Event{} = event} <- Db.update_event(event_params) do
      render(conn, "show.json", event: event)
    end
  end

  def update(conn, %{"meetup_event" => event_params}) do
    with {:ok, %Event{} = event} <- Db.update_event(event_params) do
      render(conn, "show.json", event: event)
    end
  end

  swagger_path(:delete) do
    delete("/api/event/{id}")
    summary("Delete an event by id")
    description("Delete an event by id")

    parameters do
      id(:path, :integer, "The id of the event", required: true)
    end

    response(204, "No content")
  end

  def delete(conn, %{"id" => id}) do
    with {:ok, %Event{}} <- Db.delete_event(id) do
      send_resp(conn, :no_content, "")
    end
  end

  def search(conn, search_params = %{}) do
    search_params = search_params |> ProperCase.to_snake_case()
    search_params = Map.put(search_params, "active", true)

    events = Db.search(search_params)
    render(conn, "index.json", events: events)
  end

  defp image_type("image/jpeg"), do: "jpg"
  defp image_type("image/png"), do: "png"
end
