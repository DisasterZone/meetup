defmodule MeetupAPIWeb.ImageController do
  use MeetupAPIWeb, :controller

  def create(conn, %{"image" => image_base64}) do
    filename = Logic.Image.upload_image("6", image_base64)

    conn
    |> put_status(:created)
    |> json(%{"filename" => filename})
  end
end
