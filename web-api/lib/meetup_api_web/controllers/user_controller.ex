defmodule MeetupAPIWeb.UserController do
  use MeetupAPIWeb, :controller
  use PhoenixSwagger

  require Logger

  alias Db
  alias Db.User
  alias Aws.Blob

  action_fallback(MeetupAPIWeb.FallbackController)

  def swagger_definitions do
    %{
      User:
        swagger_schema do
          title("User")
          description("An user that attends events")

          properties do
            oauth_id(:string, "The unique ID of User", required: true)
            initials(:string, "The User initials")
            first_name(:string, "The User first name")
            last_name(:string, "The User last name")
            email(:string, "The User email")
            alt_email(:string, "The User secondary email")
            phone(:string, "The User phone")
            image_url(:string, "The User image url from S3")
            office(:string, "The User home office")
          end

          example(%{
            oauth_id: "https://capgroup-dev.oktapreview.com:00u2pjztvzRGCYNYNBHY",
            initials: "JD",
            first_name: "John",
            last_name: "Doe",
            email: "jd@example.com",
            alt_email: "jd@other.com",
            phone: "(949) 555-1234",
            image_url: "http://example.com/image.jpg",
            office: "IRV"
          })
        end,
      Users:
        swagger_schema do
          title("Users")
          description("All users")
          type(:array)
          items(Schema.ref(:User))
        end,
      Error:
        swagger_schema do
          title("Errors")
          description("Error responses from the API")

          properties do
            error(:string, "The message of the error raised", required: true)
          end
        end
    }
  end

  swagger_path(:index) do
    get("/api/profiles")
    summary("List all Users")
    description("List all Users")
    response(200, "Ok", Schema.ref(:Users))
  end

  def index(conn, _params) do
    users = Db.get_users()
    render(conn, "index.json", users: users)
  end

  swagger_path(:show) do
    get("/api/profile/{id}")
    summary("Retrieve a user")
    description("Retrieve a user that has been saved")

    parameters do
      id(:path, :integer, "The id of the user", required: true)
    end

    response(200, "Ok", Schema.ref(:User))
    response(404, "Not found", Schema.ref(:Error))
  end

  def show(conn, %{"id" => id}) do
    user = Db.get_user_by_id(id)
    render(conn, "show.json", user: user)
  end

  swagger_path(:show_self) do
    get("/api/profile")
    summary("Retrieve self")
    description("Retrieve the logged in user profile")

    response(200, "Ok", Schema.ref(:User))
    response(404, "Not found", Schema.ref(:Error))
  end

  def show_self(conn, _params) do
    name_params = Logic.User.parse_name(conn.assigns.user_token["name"])

    user_params =
      Map.put(%{}, "oauth_id", create_oauth_id(conn))
      |> Map.put("email", conn.assigns.user_token["preferred_username"])

    user_params = Map.merge(user_params, name_params)

    existing_user = Db.get_user_by_oauth_id(user_params["oauth_id"])
    if Enum.count(existing_user) == 1 do
      render(conn, "show.json", user: hd(existing_user))
    else
      redirect(conn, "/")
    end
  end

  swagger_path(:create) do
    post("/api/profile/")
    summary("Add a new user")
    description("Record a new user request")

    parameters do
      user(:body, Schema.ref(:User), "User to record", required: true)
    end

    response(201, "Ok", Schema.ref(:User))
    response(422, "Unprocessable Entity", Schema.ref(:Error))
  end

  def create(conn, %{"user" => user_params}) do
    user_params = Map.put(user_params, "oauth_id", create_oauth_id(conn))

    with {:ok, %User{} = user} <- Db.create_user(user_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", user_path(conn, :show, user))
      |> render("show.json", user: user)
    end
  end

  swagger_path(:create_default) do
    post("/api/profile/login")
    summary("Create a default new user")
    description("Record a default new user request")

    parameters do
      user(:body, Schema.ref(:User), "Default user to record", required: true)
    end

    response(201, "Ok", Schema.ref(:User))
    response(422, "Unprocessable Entity", Schema.ref(:Error))
  end

  def create_default(conn, %{}) do
    name_params = Logic.User.parse_name(conn.assigns.user_token["name"])

    user_params =
      Map.put(%{}, "oauth_id", create_oauth_id(conn))
      |> Map.put("email", conn.assigns.user_token["preferred_username"])

    user_params = Map.merge(user_params, name_params)

    existing_user = Db.get_user_by_oauth_id(user_params["oauth_id"])

    if Enum.count(existing_user) == 1 do
      user = hd(existing_user)

      conn
      |> put_resp_header("location", user_path(conn, :show, user))
      |> render("show.json", user: user)
    else
      with {:ok, %User{} = user} <- Db.create_user(user_params) do
        conn
        |> put_status(:created)
        |> put_resp_header("location", user_path(conn, :show, user))
        |> render("show.json", user: user)
      end
    end
  end

  swagger_path(:update) do
    put("/api/profile/")
    summary("Update an existing user")
    description("Update an existing user")

    parameters do
      user(:body, Schema.ref(:User), "User with updates", required: true)
    end

    response(201, "Ok", Schema.ref(:User))
    response(422, "Unprocessable Entity", Schema.ref(:Error))
  end

  def update(conn, %{"file" => file_upload, "profile" => user_params}) do
    user_params = Map.put(Poison.decode!(user_params, []), "oauth_id", create_oauth_id(conn))
    user_params = ProperCase.to_snake_case(user_params)

    user_id = user_params["id"]
    {:ok, image_file} = File.read(file_upload.path)

    {:ok, image} =
      Blob.put_blob(
        "cgmeetup-profiles.s3.amazonaws.com",
        image_file,
        "#{user_id}.#{image_type(file_upload.content_type)}"
      )

    image_url = "https://s3-us-west-2.amazonaws.com/cgmeetup-profiles/cgmeetup-profiles.s3.amazonaws.com/#{user_id}.#{image_type(file_upload.content_type)}"
    user_params = Map.put(user_params, "image_url", image_url)

    with {:ok, %User{} = user} <- Db.update_user(user_params) do
      render(conn, "show.json", user: user)
    end
  end

  def update(conn, %{"params" => user_params}) do
    user_params = Map.put(Poison.decode!(user_params, []), "oauth_id", create_oauth_id(conn))

    with {:ok, %User{} = user} <- Db.update_user(user_params) do
      render(conn, "show.json", user: user)
    end
  end

  defp create_oauth_id(conn) do
    "#{conn.assigns.user_token["iss"]}:#{conn.assigns.user_token["sub"]}"
  end

  defp image_type("image/jpeg"), do: "jpg"
  defp image_type("image/png"), do: "png"
end
