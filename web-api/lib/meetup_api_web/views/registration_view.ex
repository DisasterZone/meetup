defmodule MeetupAPIWeb.RegistrationView do
  use MeetupAPIWeb, :view
  alias MeetupAPIWeb.RegistrationView

  def render("index.json", %{registrations: registrations}) do
    %{data: render_many(registrations, RegistrationView, "registration.json")}
  end

  def render("show.json", %{registration: registration}) do
    %{data: render_one(registration, RegistrationView, "registration.json")}
  end

  def render("registration.json", %{registration: registration}) do
    %{
      id: registration.id,
      event_id: registration.event_id,
      oauth_id: registration.oauth_id,
      create_datetime: registration.create_datetime,
      update_datetime: registration.update_datetime,
      waitlist_datetime: registration.waitlist_datetime
    }
  end
end
