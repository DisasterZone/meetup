defmodule MeetupAPIWeb.EventView do
  use MeetupAPIWeb, :view
  alias MeetupAPIWeb.EventView

  def render("index.json", %{events: events}) do
    render_many(events, EventView, "event.json")
  end

  def render("show.json", %{event: event}) do
    render_one(event, EventView, "event.json")
  end

  def render("event.json", %{event: event}) do
    %{
      id: event.id,
      title: event.title,
      organizer: event.organizer,
      image_url: event.image_url,
      category: event.category,
      location: event.location,
      online: event.online,
      start_time: event.start_time,
      end_time: event.end_time,
      min_participants: event.min_participants,
      max_participants: event.max_participants,
      tags: event.tags,
      description: event.description,
      active: event.active
    }
  end
end
