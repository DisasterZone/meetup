defmodule MeetupAPIWeb.UserView do
  use MeetupAPIWeb, :view
  alias MeetupAPIWeb.UserView

  def render("index.json", %{users: users}) do
    %{data: render_many(users, UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    render_one(user, UserView, "user.json")
  end

  def render("user.json", %{user: user}) do
    %{
      id: user.id,
      initials: user.initials,
      last_name: user.last_name,
      first_name: user.first_name,
      email: user.email,
      alt_email: user.alt_email,
      phone: user.phone,
      image_url: user.image_url,
      office: user.office
    }
  end
end
