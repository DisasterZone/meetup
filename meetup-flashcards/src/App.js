import React, { Component } from 'react';
import './App.css';
import Card from './Card/Card';
import DrawButton from './DrawButton/DrawButton';

class App extends Component {
  constructor(props) {
    super(props);

    this.updateCard = this.updateCard.bind(this);

    this.state = {
      cards: [ { id: 1, eng: "English", han: "Hanzi", pin: "Pinyin"} ],
      currentCard: {
      }
    }
  }

  componentWillMount() {
    console.log(this.fetchCards());

    const currentCards = this.state.cards;

    this.setState( {
      cards: currentCards,
      currentCard: this.getRandomCard(currentCards)
    })
  }

  fetchCards = () => {
    fetch('/cards.json')
    .then(rsp => rsp.json())
    .then(cards => this.setState({ cards: cards }));
  }

  getRandomCard(currentCards) {
    var card = currentCards[Math.floor(Math.random() * currentCards.length)];
    return(card);
  }

  updateCard() {
    const currentCards = this.state.cards;
    this.setState({
      currentCard: this.getRandomCard(currentCards)
    })
  }

  render() {
    return (
      <div className="App">
        <div className="cardRow">
          <Card eng={this.state.currentCard.eng}
                han={this.state.currentCard.han}
                pin={this.state.currentCard.pin}/>
        </div>
        <div className="buttonRow">
          <DrawButton drawCard={this.updateCard}/>
        </div>
        {/*<div className="cardRow">
          <ul>
            {this.state.cards.map(card =>
              <li key={card.id}><Card eng={card.eng}/></li>
            )}
          </ul>
          </div>*/}
      </div>
    );
  }
}

export default App;
