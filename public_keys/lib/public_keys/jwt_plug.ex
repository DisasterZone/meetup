defmodule PublicKeys.JwtPlug do
  import Plug.Conn

  def init(options), do: options

  def call(conn, _options) do
    token = extract_token(get_req_header(conn, "authorization"))

    conn
    |> verify_jwt(token)
    |> save_token()
    |> validate_time()
    |> generate_response()
  end

  defp extract_token(["Bearer " <> token]), do: {:ok, token}
  defp extract_token(_), do: {:error, "Missing Bearer"}

  defp save_token({:ok, conn, decoded_token}) do
    conn = assign(conn, :user_token, decoded_token)
    {:ok, conn, decoded_token}
  end

  defp save_token({:error, conn, message}), do: {:error, conn, message}

  defp verify_jwt(conn, {:ok, token}) do
    case PublicKeys.Okta.verify_jwt(token) do
      {:error, message} -> {:error, conn, message}
      {:ok, content} -> {:ok, conn, content}
    end
  end

  defp verify_jwt(conn, {:error, message}), do: {:error, conn, message}

  # Based off current datetime, determine whether the token
  # is still valid or should be rejected.
  defp validate_time({:ok, conn, decoded_token}) do
    current_time = DateTime.utc_now() |> DateTime.to_unix()

    case decoded_token["iat"] < current_time and current_time < decoded_token["exp"] do
      false -> {:error, conn, "Expired Token"}
      true -> {:ok, conn, decoded_token}
    end
  end

  defp validate_time({:error, conn, message}), do: {:error, conn, message}

  # Trim the extra information and send back the expected Conn for the
  # plug contract. If :error then adjust the status
  defp generate_response({:ok, conn, _decoded_token}), do: conn

  defp generate_response({:error, conn, message}) do
    conn
    |> send_resp(:unauthorized, message)
    |> halt()
  end
end
