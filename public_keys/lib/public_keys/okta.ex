defmodule PublicKeys.Okta do
  @url Application.get_env(:public_keys, :pk_url)
  @me __MODULE__

  import Joken

  def start_link() do
    Agent.start_link(&retrieve_public_keys/0, name: @me)
  end

  def kid_key(kid) do
    {:ok, key} = Agent.get(@me, &filter_key(&1, kid))
    key
  end

  def verify_jwt(token_payload) do
    header =
      token_payload
      |> token()
      |> peek_header()

    kid = header["kid"]
    pk = kid_key(kid)

    token_payload
    |> Joken.token()
    |> Joken.verify!(Joken.rs256(pk), [])
  end

  def filter_key(public_keys, kid) do
    match = Enum.filter(public_keys["keys"], fn x -> x["kid"] == kid end)

    cond do
      match == [] -> {:error, "No match"}
      true -> {:ok, match |> hd}
    end
  end

  def retrieve_public_keys() do
    response = HTTPotion.get!(@url)
    keys(response.body)
  end

  def keys(json) do
    Poison.decode!(json)
  end
end
