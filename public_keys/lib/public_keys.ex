defmodule PublicKeys do
  alias PublicKeys.Okta

  defdelegate kid_key(kid), to: Okta
  defdelegate verify_jwt(token_payload), to: Okta
end
