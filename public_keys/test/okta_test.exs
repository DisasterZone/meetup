defmodule OktaTest do
  use ExUnit.Case
  doctest PublicKeys.Okta

  alias PublicKeys.Okta

  # Decide how to test remove calls at some point?
  #  test "public decode" do
  #
  #    result = PublicKeys.retrieve_public_keys()
  #
  #    assert 2       == length(result["keys"])
  #    assert "RS256" == hd(result["keys"]).alg
  #    assert "AQAB"  == hd(result["keys"]).e
  #    assert "RSA"   == hd(result["keys"]).kty
  #    assert "sig"   == hd(result["keys"]).use
  #  end

  test "sample decode" do
    result = Okta.keys(okta_sample())

    assert 2 == length(result["keys"])
    assert "RS256" == hd(result["keys"])["alg"]
    assert "AQAB" == hd(result["keys"])["e"]
    assert "RSA" == hd(result["keys"])["kty"]
    assert "sig" == hd(result["keys"])["use"]
  end

  test "find kid" do
    keys = Okta.keys(okta_sample())
    {:ok, result} = Okta.filter_key(keys, "PfHZ6vuVteM1k5hTFnVhZziNBWCrzbFTHTWiqusWEHM")
    assert "RS256" == result["alg"]
  end

  test "verify token" do
    sample_token =
      "eyJraWQiOiJjajNfUzhpUFlSN0lhMU9UYzNzN29feDFaWTlmeDRqbDBFZ00ydTZRZkwwIiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiIwMHUycGp6dHZ6UkdDWU5ZTkJIWSIsIm5hbWUiOiJDaHJpcyBMdWZmIChDR0wpIiwidmVyIjoxLCJpc3MiOiJodHRwczovL2NhcGdyb3VwLWRldi5va3RhcHJldmlldy5jb20iLCJhdWQiOiIwb2FiMDZ0cnR0dVI3TFUxZDBoNyIsImlhdCI6MTUxMjQ1MTAyMSwiZXhwIjoxNTEyNDU0NjIxLCJqdGkiOiJJRC5DN0Z4UW5jTE9kOE41Ny11WV9DbGh5RWxhV3UxLVdZRU9tcHduUmh4RFBZIiwiYW1yIjpbInB3ZCJdLCJpZHAiOiIwMG8ybTk4eGxwUldDWEtXU0NBUCIsIm5vbmNlIjoiYjJlMmM0MTgxMDBkNDg1MDhmYmVlYTI2NzhjYmY5ZjUiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJDR0xAY2d1c2VyLmNhcGdyb3VwLmNvbSIsImF1dGhfdGltZSI6MTUxMjQ1MTAyMCwiYXRfaGFzaCI6InBCNDNaMkZKXzNiaDFxY2F2aWh6LXcifQ.QizTK1a0vYc0syIHABKBktRwZYEq7PNwvWsPBrbJhHzuaidf8URk2B1teIrfxpaBOGaQkkexdvP4ecSfXXxAGoFa2qR5OoKmwR3KwCjW3mjcF8ZZVweIMHUS9v05WYjnXM4yYsqLOvJYAxq7VbRYdworTS_m6s5LOsHndh4Ne6nDDL1OqY-XEFAb_HWCWNhqUF-7pPpSyq_bb6tRsL2fG4g1V4QDyYm7nEPynV11LCZs44b5AqkO1lFH8owZ0JzRVHLCj-iqhG9QE9ufUBWktwjs1zFtbI23GTYelGjMR6TZE3_ZbQ0lMV5WTeFpb_qR-kXyO2aKfrfSiGKgFPAWjg"

    {:ok, contents} = Okta.verify_jwt(sample_token)
  end

  def okta_sample do
    """
    {
      "keys":[
        {
          "alg":"RS256",
          "e":"AQAB",
          "n":"tBH3oZPezUafhpojhuhux6NLKUQgnTcs13Tif7qQj_G4jU3PDIRTd_odatTbSwQwjwrdxe9-542HK4aPCtomQPt9bT2aV_PW_zmPx3vrNI2nZ0SOMaylS7ivJcHmSaOLhMkjpDqg1RYkbuQ7rncrYuY0gFijdL_zo7CoeaV4jxXl0HVCDV6zzfOZIQ5xCembGJXP_V0B3yNrq2Jzv9AZGF_TEGc6lI4N-Iy2O-curAVFz2sdaeHwXi5umj8hnc2rJFLgEgXuGUbQHrhfa0WSkEAXgf89SHA7PqPwCntYTgHqhgTtKl5NVKijJOPYfsEpgPYgZPtQyZIkJTlInO_QFQ",
          "kid":"cj3_S8iPYR7Ia1OTc3s7o_x1ZY9fx4jl0EgM2u6QfL0",
          "kty":"RSA",
          "use":"sig"
        },
        {
          "alg":"RS256",
          "e":"AQAB",
          "n":"gEta9hK2ECzRrm9d4HrSQZYO9kRRT9Ukkk1zN_2a4QjT-ULF1eFtW9xNjk-tsKRd63WLndu67NjplNLuRhwfp7AJWtjkmqzm4QbJWhX9YXp3MQXrgTl_MUHis8ENujdxGKh1NKhb0Ef4xZiJIUBH9euYERyoUT2lXE-kJjhr9gGPcIMLTHWY_BeBu7a5IEG6CrrhfJA0SVVG1jn-2wEpc3ViTl7S674ycsnne7_9zqLSxohe1sWIaUnlXRRzFz5OzCmNxi1nfZRb0Mb5gzC2NGNXpOxNabqYyZ7Bqe498Z4aJ8rM_GPH0ibNgdYGp-BygQarmBwMEoPbdVq1vIRrhQ",
          "kid":"PfHZ6vuVteM1k5hTFnVhZziNBWCrzbFTHTWiqusWEHM",
          "kty":"RSA",
          "use":"sig"
        }
      ]
    }
    """
  end
end
