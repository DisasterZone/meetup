defmodule PublicKeys.Mixfile do
  use Mix.Project

  def project do
    [
      app: :public_keys,
      version: "0.1.0",
      elixir: "~> 1.5",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {PublicKeys.Application, []},
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpotion, "~> 3.0.2"},
      {:joken, "~> 1.5"},
      {:jsx, "~> 2.9"},
      {:plug, "~> 1.0"},
      {:poison, "~> 3.1"},
      {:quantum, "~> 2.2.7"},
      {:timex, "~> 3.0"},
      {:mix_test_watch, "~> 0.3", only: :dev, runtime: false}
    ]
  end
end
