defmodule Azure do
  defdelegate list_container(container), to: Azure.Blob
  defdelegate put_blob(container, file, blob_name), to: Azure.Blob
end
