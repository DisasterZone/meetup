defmodule Azure.Blob do
  @azure_version "2017-04-17"

  def list_container(container) do
    request_date = request_date()
    authorization_header = list_authorization_header(request_date, container)

    build_url(:list_container, container)
    |> HTTPoison.get(
      list_headers(request_date, authorization_header),
      options()
    )
  end

  def put_blob(container, file, blob_name) do
    request_date = request_date()

    authorization_header =
      put_authorization_header(request_date, container, blob_name, byte_size(file))

    build_url(:put_blob, container, blob_name)
    |> HTTPoison.put(
      file,
      put_headers(request_date, authorization_header, blob_name),
      options()
    )
  end

  def list_authorization_header(request_date, container) do
    azure_account = System.get_env("CG_MEETUP_AZURE_ACCOUNT")
    azure_access_key = System.get_env("CG_MEETUP_AZURE_ACCESS_KEY")

    {:ok, decoded_key} = azure_access_key |> Base.decode64()

    signature =
      :crypto.hmac(:sha256, decoded_key, list_signature(request_date, container))
      |> Base.encode64()

    "SharedKey #{azure_account}:#{signature}"
  end

  def put_authorization_header(request_date, container, blob_name, blob_size) do
    azure_account = System.get_env("CG_MEETUP_AZURE_ACCOUNT")
    azure_access_key = System.get_env("CG_MEETUP_AZURE_ACCESS_KEY")
    {:ok, decoded_key} = azure_access_key |> Base.decode64()

    signature =
      :crypto.hmac(
        :sha256,
        decoded_key,
        put_signature(request_date, container, blob_name, blob_size)
      )
      |> Base.encode64()

    "SharedKey #{azure_account}:#{signature}"
  end

  def list_signature(request_date, container) do
    # set canonicalized headers
    x_ms_date = "x-ms-date:#{request_date}"
    x_ms_version = "x-ms-version:#{@azure_version}"

    # assign values for string_to_sign
    verb = verb(:list_container)
    content_encoding = "\n"
    content_language = "\n"
    content_length = "\n"
    content_md5 = "\n"
    content_type = "\n"
    date = "\n"
    if_modified_since = "\n"
    if_match = "\n"
    if_none_match = "\n"
    if_unmodified_since = "\n"
    range = "\n"
    canonicalized_headers = "#{x_ms_date}\n#{x_ms_version}\n"
    canonicalized_resource = list_resource(container)

    # concat string_to_sign
    verb <>
      content_encoding <>
      content_language <>
      content_length <>
      content_md5 <>
      content_type <>
      date <>
      if_modified_since <>
      if_match <>
      if_none_match <>
      if_unmodified_since <> range <> canonicalized_headers <> canonicalized_resource
  end

  def put_signature(request_date, container, blob_name, blob_size) do
    # set canonicalized headers
    x_ms_date = "x-ms-date:#{request_date}"
    x_ms_version = "x-ms-version:#{@azure_version}"

    # assign values for string_to_sign
    verb = verb(:put_blob)
    content_encoding = "\n"
    content_language = "\n"
    content_length = "#{blob_size}\n"
    content_md5 = "\n"
    content_type = "text/plain; charset=UTF-8\n"
    date = "\n"
    if_modified_since = "\n"
    if_match = "\n"
    if_none_match = "\n"
    if_unmodified_since = "\n"
    range = "\n"

    canonicalized_headers =
      "x-ms-blob-content-disposition:attachment; filename:#{blob_name}\nx-ms-blob-type:BlockBlob\n#{
        x_ms_date
      }\nx-ms-meta-m1:v1\nx-ms-meta-m2:v2\n#{x_ms_version}\n"

    canonicalized_resource = put_resource(container, blob_name)

    # concat string_to_sign
    verb <>
      content_encoding <>
      content_language <>
      content_length <>
      content_md5 <>
      content_type <>
      date <>
      if_modified_since <>
      if_match <>
      if_none_match <>
      if_unmodified_since <> range <> canonicalized_headers <> canonicalized_resource
  end

  def verb(:list_container), do: "GET\n"
  def verb(:put_blob), do: "PUT\n"

  def list_resource(container) do
    azure_account = System.get_env("CG_MEETUP_AZURE_ACCOUNT")
    "/#{azure_account}/#{container}\ncomp:list\nrestype:container"
  end

  def put_resource(container, blob_name) do
    azure_account = System.get_env("CG_MEETUP_AZURE_ACCOUNT")
    "/#{azure_account}/#{container}/#{blob_name}"
  end

  def list_headers(request_date, authorization_header) do
    [
      "x-ms-date": request_date,
      "x-ms-version": @azure_version,
      Authorization: authorization_header
    ]
  end

  def put_headers(request_date, authorization_header, blob_name) do
    [
      "x-ms-date": request_date,
      "x-ms-version": @azure_version,
      Authorization: authorization_header,
      "Content-Type": "text/plain; charset=UTF-8",
      "Content-Length": 4,
      "x-ms-blob-content-disposition": "attachment; filename:#{blob_name}",
      "x-ms-blob-type": "BlockBlob",
      "x-ms-meta-m1": "v1",
      "x-ms-meta-m2": "v2"
    ]
  end

  def options() do
    [
      ssl: [{:versions, [:"tlsv1.2"]}],
      recv_timeout: 5000
    ]
  end

  def build_url(:list_container, container) do
    azure_account = System.get_env("CG_MEETUP_AZURE_ACCOUNT")
    "https://#{azure_account}.blob.core.windows.net/#{container}?restype=container&comp=list"
  end

  def build_url(:put_blob, container, blob_name) do
    azure_account = System.get_env("CG_MEETUP_AZURE_ACCOUNT")
    "https://#{azure_account}.blob.core.windows.net/#{container}/#{blob_name}"
  end

  @spec request_date() :: String.t()
  def request_date() do
    Timex.now()
    # Wed, 02 Aug 2017 00:52:10 +0000
    |> Timex.format!("{RFC1123}")
    # Wed, 02 Aug 2017 00:52:10 GMT
    |> String.replace("+0000", "GMT")
  end
end
