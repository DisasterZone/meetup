# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :azure,
  storage_service_version: "2017-04-17",
  account: System.get_env("CG_MEETUP_AZURE_ACCOUNT"),
  access_key: System.get_env("CG_MEETUP_AZURE_ACCESS_KEY")

config :logger, :console, format: "[$level] $message\n"
